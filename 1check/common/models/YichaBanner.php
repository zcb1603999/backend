<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yicha_banner".
 *
 * @property integer $id
 * @property integer $store_user_id
 * @property string $outlink
 * @property string $img_url
 * @property integer $orders
 * @property integer $is_use
 */
class YichaBanner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yicha_banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['outlink', 'orders', 'is_use'], 'required'],
            [['store_user_id', 'orders', 'is_use'], 'integer'],
            [['outlink'], 'string', 'max' => 256],
            [['addtime'], 'safe'],
            ['addtime', 'default','value'=>date('Y-m-d H:i:s')],
            [['img_url'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_user_id' => 'Store User ID',
            'outlink' => '跳转连接',
            'img_url' => '图片',
            'orders' => '显示顺序',
            'is_use' => '启用',
        ];
    }
    
    /**
     * 获取当前页面要用的最新数据
     */
    public function getNowData($store_user_id){
        return $this->find()->where(['store_user_id'=>$store_user_id,'is_use'=>1])->asArray()->all();
    }
    
    public static function showUse($val=null){
        $use_status=[
            0=>'未使用',
            1=>'使用中'
        ];
        if($val!==null){
            return array_key_exists($val,$use_status)?$use_status[$val]:$use_status[0];
        }else{
            return $use_status;
        }
    }
}
