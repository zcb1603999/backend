<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;


$this->params['breadcrumbs'][] = ['label' => 'Banner推荐列表', 'url' => ['index']];
$this->params['breadcrumbs'][] = '修改Banner';
?>
<div class="yicha-banner-update">

    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?> 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
