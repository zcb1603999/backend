<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\MenuselectorWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="role-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'role_name')->textInput(['maxlength' => true]) ?>
    
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <label>选择关联菜单</label>
                <?=MenuselectorWidget::widget(['selected_resources'=>$resources])?>
            </div>
            <div class="col-md-6">
                <label>选择关联用户</label>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <?php $jishu=0;foreach($user as $u){ ?>
                            <td>
                                <input type="checkbox" value="<?=$u['id']?>" name="users[]" <?=(isset($selected_user[$u['id']])&&!empty($selected_user[$u['id']])?'checked="checked"':'')?>/>&nbsp;<?=$u['username']?>
                            </td>
                            <?php $jishu++;
                            echo $jishu%2==0?'</tr>':'';
                            } 
                            echo $jishu%2!=0?'</tr>':'';
                            ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '创建' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
