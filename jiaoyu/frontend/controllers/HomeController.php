<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\UserEnroll;
use yii\bootstrap\ActiveForm;
use yii\web\Response;

class HomeController extends Controller{
   
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'enroll' => ['POST'],
                ],
            ],
        ];
    }
    
    public  function actionIndex(){
        return $this->renderPartial('index');
    }
    
    public function actionEnroll(){
        $model=new UserEnroll();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;       
            if(empty(ActiveForm::validate($model))){
                $model->addtime=date('Y-m-d H:i:s');
                if($model->save()){
                    return 'success';
                }
            }
        }else{
            //Yii::$app->response->format = Response::FORMAT_JSON;        
            //return ActiveForm::validate($model);
        }
        return "error";
    }
    
}
