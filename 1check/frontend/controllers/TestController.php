<?php

namespace frontend\controllers;

use Yii;
use common\controllers\FcommonController;
use common\models\StoreSearch;

class TestController extends FcommonController {
    public function actionElastic(){
        $search = new StoreSearch();
        $index_info=Yii::$app->params['es.yanglao.store.indexinfo'];
        $search->getTagAggs($index_info['name'], $index_info['type']);
    }
  
}
