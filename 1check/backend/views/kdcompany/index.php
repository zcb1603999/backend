<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = '快递公司列表';
?>
<div class="kd-company-index">

    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?> 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'第{page}/{pageCount}页，本页{count}条，共{totalCount}条',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'company_name',
            'company_key',
        ],
    ]); ?>
</div>
