<?php

namespace backend\controllers;

class LoginController extends \yii\web\Controller
{
    public function actionLogin()
    {
    	if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        return $this->render('login');
    }
	
	public function actionLogout(){
		
	}

}
