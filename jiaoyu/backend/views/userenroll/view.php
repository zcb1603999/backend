<?php

use yii\widgets\DetailView;
use common\models\UserEnroll;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = ['label' => '报名列表', 'url' => ['index']];
$this->params['breadcrumbs'][] = '报名信息';
?>
<div class="user-enroll-view">
    
    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?> 

    <h1>报名信息</h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'baby_name',
            'baby_birthday',
            [
                'attribute'=>'baby_sex',
                'value'=>UserEnroll::babysexDropDown($model->baby_sex)
            ],
            'parent_name',
            'parent_phone',
            'address',
            [
                'attribute'=>'enroll_type',
                'value'=>UserEnroll::enrolltypeDropDown($model->enroll_type)
            ],
            'addtime',
        ],
    ]) ?>

</div>
