<?php

namespace backend\models;

use Yii;
use backend\models\Role;
use yii\db\Query;

/**
 * This is the model class for table "resource".
 *
 * @property integer $id
 * @property string $resource_name
 * @property string $resource_url
 * @property string $addtime
 */
class Resource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resource';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['resource_name'], 'required'],
            [['addtime'], 'safe'],
            ['addtime', 'default','value'=>date('Y-m-d H:i:s')],
            [['resource_name'], 'string', 'max' => 128],
            ['parent_id','default','value'=>0],
            ['resource_url','string','max'=>256],
            [['level'],'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'=>'编号',
            'resource_name' => '资源名称',
            'addtime'=>'添加时间',
            'resource_url'=>'资源地址',
            'level'=>'层级'
        ];
    }
	
	public function getRoles(){
            return $this->hasMany(Role::className(),['id'=>'role_id'])->viaTable('role2resource',['resource_id'=>'id']);
	}
	
	public function getUserResource($user_id){
            $query=new Query();
            return $query->select(['resource.*'])->from('resource')
            ->join('INNER JOIN','role2resource','`resource`.`id`=`role2resource`.`resource_id`')
            ->join('INNER JOIN','user2role','`user2role`.`role_id`=`role2resource`.`role_id`')
            ->where(['user2role.user_id'=>$user_id])
            ->andFilterCompare('level', '<3')
            ->orderBy(['addtime'=>SORT_ASC])
            ->all();
            return $this->render('index');
	}
        
        /**
         * 将原始菜单数据换为可用数组
         */
        public static function toArrayMenu($menuArray){
            if(empty($menuArray)){
                return [];
            }
            $resourceObj=new Resource();
            usort($menuArray,[$resourceObj,'sortmenu']);
            
            $tmp=[];
            $max_level=0;
            foreach($menuArray as $r){
                if($r['level']>$max_level){
                    $max_level=$r['level'];
                }
                $tmp[$r['level']][$r['parent_id']][]=$r;
            }
            for($i=$max_level-1;$i>0;$i--){
                foreach($tmp[$i] as $parent_id=>$t){
                    foreach($t as $k=>$v){
                        if(isset($tmp[$i+1][$v['id']])&&!empty($tmp[$i+1][$v['id']])){
                            $tmp[$i][$parent_id][$k]['submenu']=$tmp[$i+1][$v['id']];
                            unset($tmp[$i+1][$v['id']]);
                        }
                    }
                }
            }
            $menu_array=$tmp[1][0];
            $resource=new Resource();
            usort($menu_array,[$resource,'sortmenu']);
            return $tmp[1][0];
            /*$menu=[];
            foreach($menuArray as $r){
                if($r['parent_id']==0){
                    $menu[$r['id']]=$r;
                    $menu[$r['id']]['is_expend']=false;
                }elseif(isset($menu[$r['parent_id']])&&!empty($menu[$r['parent_id']])){
                    $menu[$r['parent_id']]['submenu'][]=$r;
                }
            }
            return $menu;*/
        }
        
        public function sortmenu($a,$b){
            return $a['addtime']<$b['addtime']?-1:1;
	}
}
