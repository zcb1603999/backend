<?php
namespace console\controllers;

use Yii;
use common\controllers\CcommonController;

class StoredataController extends CcommonController {
    
    /**
     * 创建新索引
     */
    public function actionCreateindex(){
        $index_info=Yii::$app->params['es.yanglao.store.indexinfo'];
        $this->Createindex($index_info['name']);
        $this->PutAliasIndex($index_info['name'],$index_info['aliasname']);
    }
    
    public function actionDeleteindex(){
        $index_info=Yii::$app->params['es.yanglao.store.indexinfo'];
        $this->Deleteindex($index_info['name']);
    }
    
    public function actionPutmapping(){
        $index_info=Yii::$app->params['es.yanglao.store.indexinfo'];
        $mapping=Yii::$app->params['es.yanglao.store.mapping'];
        $this->PutMapping($index_info['name'],$index_info['type'],$mapping);
    }
    
    public function actionGetmapping(){
        $index_info=Yii::$app->params['es.yanglao.store.indexinfo'];
        $this->GetMapping($index_info['name'], $index_info['type']);
    }
    
    /**
     * 以bulk方式添加数据
     */
    public function actionAdddata(){
        $index_info=Yii::$app->params['es.yanglao.store.indexinfo'];
        $index_info['name'];
        $index_info['type'];
        $data=[
            [
                'id'=>1,
                'store_name'=>'普乐园丰台养老院',
                'tag'=>[
                    [
                        'id'=>'1',
                        'name'=>'自理',
                        'tag_type_id'=>'3',
                        'tag_type_name'=>'收住对象'
                    ],
                    [
                        'id'=>'10',
                        'name'=>'可接收异地老人',
                        'tag_type_id'=>'4',
                        'tag_type_name'=>'特色服务'
                    ],
                ],
            ],
            [
                'id'=>2,
                'store_name'=>'江苏丹阳市红叶颐馨园',
                'tag'=>[
                    [
                        'id'=>'4',
                        'name'=>'特护',
                        'tag_type_id'=>'3',
                        'tag_type_name'=>'收住对象'
                    ],
                    [
                        'id'=>'11',
                        'name'=>'医保定点',
                        'tag_type_id'=>'4',
                        'tag_type_name'=>'特色服务'
                    ],
                ],
            ],
            [
                'id'=>3,
                'store_name'=>'福提园养老院',
                'tag'=>[
                    [
                        'id'=>'4',
                        'name'=>'特护',
                        'tag_type_id'=>'3',
                        'tag_type_name'=>'收住对象'
                    ],
                    [
                        'id'=>'11',
                        'name'=>'医保定点',
                        'tag_type_id'=>'4',
                        'tag_type_name'=>'特色服务'
                    ],
                ],
            ],
            [
                'id'=>4,
                'store_name'=>'北京光大汇晨古塔老年公寓',
                'tag'=>[
                    [
                        'id'=>'1',
                        'name'=>'自理',
                        'tag_type_id'=>'3',
                        'tag_type_name'=>'收住对象'
                    ],
                    [
                        'id'=>'10',
                        'name'=>'可接收异地老人',
                        'tag_type_id'=>'4',
                        'tag_type_name'=>'特色服务'
                    ],
                ],
            ]
        ];
        $this->PutData($index_info['name'],$index_info['type'],$data);
    }
    
}
