<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use common\models\YichaNewproduct;

$this->params['breadcrumbs'][] = '新品推荐列表';
?>
<div class="yicha-newproduct-index">
    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?> 
    <p>
        <?= Html::a('创建新品', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'第{page}/{pageCount}页，本页{count}条，共{totalCount}条',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label'=>'图片',
                'format'=>[
                    'image',
                    [
                        'width'=>'170',
                        'height'=>'120'
                    ]
                ],
                'value' => function ($model) { 
                    return Yii::getAlias('@image_domain').$model->img_url; 
                }
            ],
            'outlink',
            'orders',
            [
                'attribute'=>'is_use',
                'value'=>function($model){
                    return YichaNewproduct::showUse($model->is_use);
                },
                'filter'=>YichaNewproduct::showUse()
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作', 
                'template'=>'{update} {delete}',
            ],
        ],
    ]); ?>
</div>
