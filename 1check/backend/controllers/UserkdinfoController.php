<?php

namespace backend\controllers;

use Yii;
use common\models\UserKdInfo;
use common\models\UserKdInfoSearch;
use common\controllers\BcommonController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use moonland\phpexcel\Excel;
use common\models\KdCompany;
use common\models\YichaExcelfile;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * UserkdinfoController implements the CRUD actions for UserKdInfo model.
 */
class UserkdinfoController extends BcommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionDaoexcel(){
        //set_time_limit(600);
        $model=new YichaExcelfile();
        if ($model->load(Yii::$app->request->post())) {
            $model->file_path=UploadedFile::getInstance($model, 'file_path');
            if ($model->file_path && $model->validate()) {
                $model->file_path=$this->saveFileToServer($model,'file_path','excel');
                $model->save();
                $this->importExcelData($model->id);
            }else{
                $model=new YichaExcelfile();
                return $this->redirect('daoexcel',[
                    'model'=>$model
                ]);
            }
        }
        return $this->render('daoexcel',[
            'model'=>$model
        ]);
    }
    
    private function importExcelData($file_id){
        $file=YichaExcelfile::findOne($file_id);
        $data=Excel::import(Yii::getAlias('@frontend_webroot').$file->file_path,[
            'setFirstRecordAsKeys'=>true,
        ]);
        $kd=$data[0];
        $rule=UserKdInfo::ColNameRule();
        $model_kdcomp=new KdCompany();
        $kd_comp=$model_kdcomp->getNameAsKey();
        $check_result=$this->checkExcelData($kd,$rule,$kd_comp);
        if($check_result['status']=='success'){
            $connection = Yii::$app->db;
            $insert_arr=[];
            $num=0;
            foreach($kd as $k=>$v){
                foreach($rule as $key_name=>$r){
                    if(isset($v[$key_name])){
                        if($key_name=='快递公司'){
                            $insert_arr[$num][]=$kd_comp[$v[$key_name]]['id'];
                        }else{
                            $insert_arr[$num][]=$v[$key_name];
                        }
                    }else{
                        $insert_arr[$num][]='';
                    }
                }
                //$insert_arr[$num][]=Yii::$app->user->id;
                $insert_arr[$num][]=1;//测试用
                if($k%200==0){
                    //$connection->createCommand()->batchInsert('user_kd_info',['user_name','phone','kd_number','kd_company_id','send_user','province','city','area','address','product_name','remark','store_user_id'],$insert_arr)->execute();
                    //$insert_arr=[];
                    $num=0;
                }
                $num++;
            }
            print_r($insert_arr);
            die;
        }else{
            Yii::$app->getSession()->setFlash('error', $check_result['reason']);
        }
    }
    
    private function checkExcelData(&$data,$rule,$kd_comp){
        $result=[
            'status'=>'success',
            'reason'=>''
        ];
        foreach($rule as $name=>$con){
            if($con=='R'){
                if(isset($data[0][$name])){
                    continue;
                }else{
                    $result=[
                        'status'=>'error',
                        'reason'=>$name.' 没有这一列!'
                    ];
                    return $result;
                }
            }
        }
        $ndata=[];
        foreach($data as $k=>$v){
            foreach($v as $col_name=>$col_val){
                $col_val=trim($col_val);
                
                if($col_name=='快递公司'){
                    if(isset($kd_comp[$col_val])&&!empty($kd_comp[$col_val])){
                        $result=[
                            'status'=>'error',
                            'reason'=>'第'.($k+1).'行，快递公司名字不正确,请参考正确的名称!.<a href="'.Url::to(['kdcompany/index']).'" class="alert-link">...</a>'
                        ];
                        return $result;
                    }
                }
                
                if($rule[$col_name]=='R'){
                    if(empty($col_val)){
                        $result=[
                            'status'=>'error',
                            'reason'=>'第'.($k+1).'行，'.$col_name.' 不可为空!'
                        ];
                        return $result;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Lists all UserKdInfo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserKdInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserKdInfo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserKdInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserKdInfo();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserKdInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserKdInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserKdInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserKdInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserKdInfo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
