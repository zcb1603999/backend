<?php
use yii\helpers\Url;
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<?php $mcount=1;foreach($menu as $m) { ?>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading<?=$mcount?>">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$mcount?>" aria-expanded="true" aria-controls="collapse<?=$mcount?>">
					<?=$m['resource_name']?>
				</a>
			</h4>
		</div>
		<div id="collapse<?=$mcount?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?=$mcount?>">
			<div class="list-group">
				<?php 
				if(isset($m['submenu'])){
					foreach($m['submenu'] as $v){
				?>
				<a href="<?=Url::to([$v['resource_url']])?>" class="list-group-item">
					&nbsp;&nbsp;<?=$v['resource_name']?>
				</a>
				<?php }} ?>
			</div>
		</div>
	</div>
<?php $mcount++;} ?>
</div>