<?php
namespace console\controllers;

use Yii;
use common\controllers\CcommonController;
use common\models\UserKdInfo;
use common\models\KdCompany;


class KdniaoController extends CcommonController{
    
    public function actionTracesub(){
        
        $model=new UserKdInfo();
        $need=$model->getNeedSubscribeData();
        $kd_company=new KdCompany();
        
        foreach ($need as $v){
            $requestData=[
                'ShipperCode'=>$v['kd_number'],
                'LogisticCode'=>$kd_company[$v['kd_company_id']]['key'],
                'OrderCode'=>$v['id']
            ];
            $requestDataJson=json_encode($requestData);
            $datas=[
                'EBusinessID' => Yii::$app->params['kdniao.EBusinessID'],
                'RequestType' => '1008',
                'RequestData' => urlencode($requestData),
                'DataType' => '2',
                'DataSign'=>$this->encrypt($requestDataJson,Yii::$app->params['kdniao.AppKey'])
            ];
            $result_json=$this->sendPost(Yii::$app->params['kdniao.ReqURL'], $datas);
            if($result_json){
                $result=json_decode($result_json,true);
                if(isset($result['Success'])&&$result['Success']=='true'){
                    $one_data=$model->find($v['id']);
                    $one_data->is_subscribe=1;
                    $one_data->save();
                }
            }
        }
        
        
    }
    
    private function sendPost($url, $datas){
        $temps = array();	
        foreach ($datas as $key => $value) {
            $temps[] = sprintf('%s=%s', $key, $value);		
        }	
        $post_data = implode('&', $temps);
        $url_info = parse_url($url);
        if(empty($url_info['port'])){
            $url_info['port']=80;	
        }
        $httpheader = "POST " . $url_info['path'] . " HTTP/1.0\r\n";
        $httpheader.= "Host:" . $url_info['host'] . "\r\n";
        $httpheader.= "Content-Type:application/x-www-form-urlencoded\r\n";
        $httpheader.= "Content-Length:" . strlen($post_data) . "\r\n";
        $httpheader.= "Connection:close\r\n\r\n";
        $httpheader.= $post_data;
        $fd = fsockopen($url_info['host'], $url_info['port']);
        fwrite($fd, $httpheader);
        $gets = "";
        $headerFlag = true;
        while (!feof($fd)) {
            if (($header = @fgets($fd)) && ($header == "\r\n" || $header == "\n")) {
                break;
            }
        }
        while (!feof($fd)) {
            $gets.= fread($fd, 128);
        }
        fclose($fd);  

        return $gets;
    }
    
    private function encrypt($data, $appkey){
        return urlencode(base64_encode(md5($data.$appkey)));
    }
    
}
