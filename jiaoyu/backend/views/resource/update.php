<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Resource */

$this->title = '修改菜单: ';
$this->params['breadcrumbs'][] = ['label' => '菜单管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="resource-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'roles'=>$roles,
        'parent_id'=>$parent_id,
        'level'=>$level,
        'selected_roles'=>$selected_roles
    ]) ?>

</div>
