<?php
namespace console\controllers;

use common\controllers\CcommonController;

class TestController extends CcommonController {
    public function actionCreate(){
        $params=[];
        for($i = 0; $i < 3; $i++) {
            $params['body'][] = [
                'index' => [
                    '_index' => 'my_index',
                    '_type' => 'my_type',
                ]
            ];

            $params['body'][] = [
                'my_field' => 'my_value',
                'second_field' => 'some more values'
            ];
        }
        
        print_r($params);
    }
}
