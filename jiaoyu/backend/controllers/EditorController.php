<?php
namespace backend\controllers;
use Yii;
use crazydb\ueditor\UEditorController;

class EditorController extends UEditorController{
    public function init(){
        parent::init();
        $this->config['imagePathFormat']='/img/zw/wechat/{yyyy}{mm}{dd}/{time}{rand:8}';
    }
}
