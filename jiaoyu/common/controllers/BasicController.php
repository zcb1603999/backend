<?php
namespace common\controllers;

use yii\web\Controller;
use common\controllers\UtilTool;

class BasicController extends Controller{
    
    use UtilTool;
    public function init(){
        UtilTool::initObj();
    }
}
