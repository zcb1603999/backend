<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserKdInfo;

/**
 * UserKdInfoSearch represents the model behind the search form about `common\models\UserKdInfo`.
 */
class UserKdInfoSearch extends UserKdInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_user_id', 'kd_company_id', 'kd_status', 'is_subscribe'], 'integer'],
            [['user_name', 'phone', 'province', 'city', 'area', 'address', 'kd_number', 'product_name', 'send_user', 'remark', 'addtime', 'kd_info'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserKdInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_user_id' => $this->store_user_id,
            'kd_company_id' => $this->kd_company_id,
            'addtime' => $this->addtime,
            'kd_status' => $this->kd_status,
            'is_subscribe' => $this->is_subscribe,
        ]);

        $query->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'province', $this->province])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'kd_number', $this->kd_number])
            ->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'send_user', $this->send_user])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'kd_info', $this->kd_info]);

        return $dataProvider;
    }
}
