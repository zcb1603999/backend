<?php

namespace backend\controllers;

use Yii;
use backend\models\Role;
use backend\models\RoleSearch;
use common\controllers\BcommonController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\User;

/**
 * RoleController implements the CRUD actions for Role model.
 */
class RoleController extends BcommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Role models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Role model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $resourceObj=$model->resources;
        foreach($resourceObj as $v){
            $resources[$v['id']]=$v['id'];
        }
        $user=$model->users;
        return $this->render('view', [
            'model' => $model,
            'resources'=>$resources,
            'users'=>$user
        ]);
    }

    /**
     * Creates a new Role model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Role();
        $user_model=new User();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->createRelationResource(['role'=>$model->id],Yii::$app->request->post('resources'));
            $user_model->createRelationRole(['role'=>$model->id], Yii::$app->request->post('users'));
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $user=$user_model->find()->asArray()->all();
            return $this->render('create', [
                'model' => $model,
                'user'=>$user,
                'selected_user'=>[]
            ]);
        }
    }

    /**
     * Updates an existing Role model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $role_model=new Role();
        $user_model=new User();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $role_model->createRelationResource(['role'=>$model->id],Yii::$app->request->post('resources'));
            $user_model->createRelationRole(['role'=>$model->id], Yii::$app->request->post('users'));
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $resourceObj=$model->resources;
            foreach($resourceObj as $v){
                $resources[$v->id]=$v->id;
            }
            $user=$user_model->find()->asArray()->all();
            
            $selected_user=[];
            $userObj=$model->users;
            foreach($userObj as $v){
                $selected_user[$v->id]=$v->id;
            }
            return $this->render('update', [
                'model' => $model,
                'resources'=>$resources,
                'user'=>$user,
                'selected_user'=>$selected_user
            ]);
        }
    }

    /**
     * Deletes an existing Role model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Role model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Role the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Role::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
