<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\YichaRecommandProduct */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Yicha Recommand Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yicha-recommand-product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pname',
            'pintro',
            'price',
            'image_url:url',
            'is_use',
        ],
    ]) ?>

</div>
