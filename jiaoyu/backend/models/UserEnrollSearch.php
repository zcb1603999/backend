<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserEnroll;

/**
 * UserEnrollSearch represents the model behind the search form about `common\models\UserEnroll`.
 */
class UserEnrollSearch extends UserEnroll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'baby_sex', 'enroll_type'], 'integer'],
            [['baby_name', 'baby_birthday', 'parent_name', 'parent_phone', 'address', 'addtime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserEnroll::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'baby_birthday' => $this->baby_birthday,
            'baby_sex' => $this->baby_sex,
            'enroll_type' => $this->enroll_type,
            'addtime' => $this->addtime,
        ]);

        $query->andFilterWhere(['like', 'baby_name', $this->baby_name])
            ->andFilterWhere(['like', 'parent_name', $this->parent_name])
            ->andFilterWhere(['like', 'parent_phone', $this->parent_phone])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
