<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kd_company".
 *
 * @property integer $id
 * @property string $company_name
 * @property string $company_key
 */
class KdCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kd_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_name', 'company_key'], 'required'],
            [['company_name', 'company_key'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_name' => '名称',
            'company_key' => '简称',
        ];
    }
    
    public function getAll(){
        $data=$this->find()->asArray()->all();
        $return_data=[];
        foreach($data as $v){
            $return_data[$v['id']]=[
                'name'=>$v['company_name'],
                'key'=>$v['company_key']
            ];
        }
        return $return_data;
    }
    
    /**
     * 用快递公司名字作为key的数组
     */
    public function getNameAsKey(){
        $data=$this->find()->asArray()->all();
        $return_data=[];
        foreach($data as $v){
            $return_data[$v['company_name']]=[
                'id'=>$v['id'],
                'key'=>$v['company_key']
            ];
        }
        return $return_data;
    }
}
