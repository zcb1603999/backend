<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserKdInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-kd-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'store_user_id') ?>

    <?= $form->field($model, 'user_name') ?>

    <?= $form->field($model, 'phone') ?>

    <?= $form->field($model, 'province') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'kd_number') ?>

    <?php // echo $form->field($model, 'kd_company_id') ?>

    <?php // echo $form->field($model, 'product_name') ?>

    <?php // echo $form->field($model, 'send_user') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'addtime') ?>

    <?php // echo $form->field($model, 'kd_status') ?>

    <?php // echo $form->field($model, 'kd_info') ?>

    <?php // echo $form->field($model, 'is_subscribe') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
