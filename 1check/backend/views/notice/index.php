<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = '公告列表';
?>
<div class="yicha-notice-index">
    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?> 
    
    <p>
        <?= Html::a('新建公告', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'第{page}/{pageCount}页，本页{count}条，共{totalCount}条',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'content',
            'orders',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作', 
                'template'=>'{update} {delete}',
            ],
        ],
    ]); ?>
</div>
