<div style="width: 400px;height: 700px; overflow-y: scroll;overflow-x: auto;border:1px solid #c2c2c2;padding:5px;margin:5px;">
    <div class="panel-group" id="selector_accordion" role="tablist" aria-multiselectable="true">
        <?php $mcount=1;foreach($menu as $m) { ?>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="selecor_heading<?=$mcount?>">
                    <h4 class="panel-title">
                        <input type="checkbox" class="m_selector" name="resources[]" value="<?=$m['id']?>" selfid="<?=$m['id']?>" ch_parent="" <?=(isset($selected_resources[$m['id']])&&!empty($selected_resources[$m['id']])?'checked="checked"':'')?> />&nbsp;<?=$m['resource_name']?>
                    </h4>
                </div>
                <div id="selecor_collapse<?=$mcount?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="selecor_heading<?=$mcount?>">
                    <div class="list-group">
                        <?php 
                        if(isset($m['submenu'])){
                            foreach($m['submenu'] as $v){
                        ?>
                        <span class="list-group-item">&nbsp;&nbsp;<input type="checkbox" name="resources[]" value="<?=$v['id']?>" class="m_selector" selfid="<?=$v['id']?>" ch_parent="_<?=$v['parent_id']?>_" <?=(isset($selected_resources[$v['id']])&&!empty($selected_resources[$v['id']])?'checked="checked"':'')?> />&nbsp;<?=$v['resource_name']?></span>
                        <?php if(isset($v['submenu'])) { ?>
                        <div style="padding:0 20px 0 20px;">
                        <table class="table table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th colspan="2">操作权限</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php $jishu=0;foreach($v['submenu'] as $vv) {?>
                                        <td>&nbsp;&nbsp;<input type="checkbox" name="resources[]" value="<?=$vv['id']?>" class="m_selector" selfid="<?=$vv['id']?>" ch_parent="_<?=$vv['parent_id']?>_<?=$v['parent_id']?>_" <?=(isset($selected_resources[$vv['id']])&&!empty($selected_resources[$vv['id']])?'checked="checked"':'')?> />&nbsp;<?=$vv['resource_name']?></td>
                                    <?php $jishu++;} 
                                    if($jishu<2){
                                        echo "<td></td></tr>";
                                    }else{
                                        echo "</tr>";
                                    }
                                    ?>
                            </tbody>
                        </table>
                        </div>
                        <?php } ?>
                        <?php }} ?>
                    </div>
                </div>
            </div>
        <?php $mcount++;} ?>   
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('.m_selector').click(function(){
            var me=$(this);
            var ischecked=me.is(":checked");
            var parent_id_str=me.attr('ch_parent');
            var parent_ids=parent_id_str.split('_');
            var id=me.val();
            for(k=0;k<parent_ids.length;k++){
                parent_id=parent_ids[k];
                if(parent_id){
                    if(ischecked){
                        p_ischecked=ischecked;
                    }else{
                        var has=$(".m_selector[ch_parent*='_"+parent_id+"_']:checked").not(".m_selector[ch_parent*='_"+id+"_']").length;
                        if(has>=1){
                            p_ischecked=true;
                        }else{
                            p_ischecked=false;
                        }
                    }
                    console.log(".m_selector[selfid*='"+parent_id+"']");
                    $(".m_selector[selfid='"+parent_id+"']").prop("checked",p_ischecked);
                }
            }
            if(id){
                $(".m_selector[ch_parent*='_"+id+"_']").prop("checked",ischecked);
            }
        });
    });
</script>