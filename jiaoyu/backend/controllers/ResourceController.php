<?php

namespace backend\controllers;

use Yii;
use backend\models\Resource;
use backend\models\ResourceSearch;
use common\controllers\BcommonController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Role;
use yii\data\Pagination;

/**
 * ResourceController implements the CRUD actions for Resource model.
 */
class ResourceController extends BcommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Resource models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ResourceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setPagination($this->pagination);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Resource model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $dataProvider=[];
        if($model->id){
            $searchModel = new ResourceSearch();
            $dataProvider = $searchModel->searchSub($model->id);
            $dataProvider->setPagination($this->pagination);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Resource model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id=0)
    {
        $model = new Resource();
	
        $role_model=new Role();
        $roles=$role_model->find()->all();
	$level=1;
	if($id>0){
            $presource=Resource::findOne($id);
            $level=intval($presource->level)+1;
        }	
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $role_model->createRelationResource(['resource'=>$model->id],Yii::$app->request->post('roles'));
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'roles'=>$roles,
                'parent_id'=>$id,
                'level'=>$level
            ]);
        }
    }

    /**
     * Updates an existing Resource model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $role_model=new Role();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $role_model->createRelationResource(['resource'=>$model->id],Yii::$app->request->post('roles'));
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $roles=$role_model->find()->all();
            $selected_roles_model=$model->roles;
            $selected_roles=[];
            foreach($selected_roles_model as $r){
                $selected_roles[$r->id]=$r->id;
            }
            return $this->render('update', [
                'model' => $model,
                'roles'=>$roles,
                'parent_id'=>$model->parent_id,
                'level'=>$model->level,
                'selected_roles'=>$selected_roles
            ]);
        }
    }

    /**
     * Deletes an existing Resource model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Resource model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Resource the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Resource::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
