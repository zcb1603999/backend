<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ResourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '菜单管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('创建新菜单', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'第{page}/{pageCount}页，本页{count}条，共{totalCount}条',
        'columns' => [
            'id',
            'resource_name',
            'addtime',
            [
            	'class' => 'yii\grid\ActionColumn',
            	'header'=>'操作',
            	'template'=>'{view} {update} {createsub}',
            	'buttons'=>[
            		'createsub'=>function($url,$model,$key){
                            return Html::a('<span class="glyphicon glyphicon-edit"></span>',Yii::$app->urlManager->createUrl(['resource/create','id'=>$key]),[
                                'title'=>'添加子菜单',
                            ]);
            		}
            	]
            ],
        ],
    ]); ?>
</div>
