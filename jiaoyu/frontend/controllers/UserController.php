<?php

namespace frontend\controllers;

use common\controllers\FcommonController;
use backend\models\User;
use backend\models\UserWechat;

class UserController extends FcommonController{
    
    public function actionLogin(){
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
    }
    
    public function actionLoginwechat(){
        $wechatOb=Yii::$app->wechat;
        $redirect_url=Yii::$app->urlManager->createAbsoluteUrl('user/wechatcallback');
        $redirect_url='http://www.iktrips.com/xingzhi/callback.php';
        echo $wechatOb->getOauth2AuthorizeUrl($redirect_url,'iktrips','snsapi_userinfo');
    }
    
    public function actionWechatcallback($code){
        if($wechat_user=$this->wechatOb->getOauth2AccessToken($code)){
            $openid=$wechat_user['openid'];
            $userwechat=new UserWechat();
            $uwechat=$userwechat->findOne($openid);
            if($uwechat){
                $uwechat->openid=$openid;
                $uwechat->access_token=$wechat_user['access_token'];
                $uwechat->refresh_access_token=$wechat_user['refresh_token'];
                $user_info=$this->wechatOb->getSnsUserInfo($openid,$wechat_user['access_token']);
                $uwechat->user_name=$user_info['nickname'];
                $uwechat->update();
                $user=User::findOne($user_info['user_id']);
            }else{
                $userwechat->openid=$openid;
                $userwechat->access_token=$wechat_user['access_token'];
                $userwechat->refresh_access_token=$wechat_user['refresh_token'];
                $user_info=$this->wechatOb->getSnsUserInfo($openid,$wechat_user['access_token']);
                $userwechat->user_name=$user_info['nickname'];
                $userwechat->save();
                $user=new User();
                $user->username=$user_info['nickname'];
                $user->save();
            }
            Yii::$app->user->login($user,3600 * 24 * 30);
        }else{
            return $this->redirect(['user/login']);
        }
    }
    
}
