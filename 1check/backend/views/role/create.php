<?php

use yii\helpers\Html;

$this->title = '创建角色';
$this->params['breadcrumbs'][] = ['label' => '角色', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'resources'=>[],
        'user'=>$user,
        'selected_user'=>$selected_user
    ]) ?>

</div>
