<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_enroll".
 *
 * @property string $id
 * @property string $baby_name
 * @property string $baby_birthday
 * @property integer $baby_sex
 * @property string $parent_name
 * @property string $parent_phone
 * @property string $address
 * @property integer $enroll_type
 * @property string $addtime
 */
class UserEnroll extends \yii\db\ActiveRecord
{
    
    private static $droplist_baby_sex=[1=>'男',2=>'女'];
    private static $droplist_enroll_type=[1=>'预约试听',2=>'仅报名'];
    
    public static function babysexDropDown($val=null){
        $baby_sex=UserEnroll::$droplist_baby_sex;
        if($val!==null){
            return array_key_exists($val,$baby_sex)?$baby_sex[$val]:false;
        }else{
            return $baby_sex;
        }
    }
    
    public static function enrolltypeDropDown($val=null){
        $enroll_type=UserEnroll::$droplist_enroll_type;
        if($val!==null){
            return array_key_exists($val,$enroll_type)?$enroll_type[$val]:false;
        }else{
            return $enroll_type;
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_enroll';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['baby_birthday', 'addtime'], 'safe'],
            [['baby_sex', 'enroll_type'], 'integer'],
            [['enroll_type','baby_name'], 'required'],
            [['baby_name', 'parent_name', 'parent_phone', 'address'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'baby_name' => '宝宝姓名',
            'baby_birthday' => '宝宝生日',
            'baby_sex' => '宝宝性别',
            'parent_name' => '家长姓名',
            'parent_phone' => '家长电话',
            'address' => '家庭地址',
            'enroll_type' => '报名类型',
            'addtime' => '添加时间',
        ];
    }
}
