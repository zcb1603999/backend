<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yicha_notice".
 *
 * @property integer $id
 * @property integer $store_user_id
 * @property integer $content
 * @property integer $orders
 */
class YichaNotice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yicha_notice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_user_id'], 'required'],
            [['store_user_id', 'content', 'orders'], 'integer'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_user_id' => 'Store User ID',
            'content' => '公告内容',
            'orders' => '顺序',
        ];
    }
    
    /**
     * 获取当前页面要用的最新数据
     */
    public function getNowData($store_user_id){
        return $this->find()->where(['store_user_id'=>$store_user_id])->orderBy(['orders'=>SORT_ASC])->limit(5)->asArray()->all();
    }
}
