<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       'css/site.css',
       //'css/fileinput.css'
    ];
    public $js = [
    	'js/bootstrap.min.js',
        'js/fileinput.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = [  
        'position' => View::POS_HEAD,
    ];
    
    public static function addScript($view, $jsfile) {  
        $view->registerJsFile($jsfile, [AppAsset::className(),'depends'=>['backend\assets\AppAsset'],'position'=>View::POS_HEAD]);  
    }
    
    public static function addCss($view, $cssfile) {  
        $view->registerCssFile($cssfile, [AppAsset::className(), 'depends' => ['backend\assets\AppAsset']]);
    }
}
