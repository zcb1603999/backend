<?php
namespace common\controllers;

class PayTool {
    
    /**
     * 微信统一下单函数
     */
    public function wxUnifiedOrder(array $order,$timeOut=6){
        /**
         * 必填参数
         */
        $need = array('out_trade_no','body','total_fee','trade_type');
        $keys = array('appid','mch_id','device_info','nonce_str','sign','detail','attach','fee_type',
            'spbill_create_ip','time_start','time_expire','goods_tag','notify_url','product_id','openid');
        $keys = array_merge($need,$keys);
        
        foreach($need as $key){
            if(empty($order[$key])) {
                throw new InvalidValueException("缺少统一下单接口必填参数{$key}！");
            }
        }
        if($order['trade_type'] == "JSAPI" && empty($order['openid'])){
            throw new InvalidValueException("统一支付接口中，缺少必填参数openid！trade_type为JSAPI时，openid为必填参数！");
        }
        
        $order = array_intersect_key($order,array_fill_keys($keys,''));
        $xml = $this->toXml($order);
        $response = $this->postXmlCurl($xml, $this->order_url, false, $timeOut);
        $result = $this->convertResponse($response);
        return $result;
    }
    
    /**
     * 解析返回响应的xml数据 
     */
    private function convertResponse($xml){
        $result = $this->fromXml($xml);
        if($result['return_code'] != 'SUCCESS'){
            throw new InvalidValueException($result['return_msg']);
        }
        if($this->checkSign($result) === true){
            return $result;
        }else{
            return false;
        }
    }
    
    /**
     * 数组转xml 
     */
    private function toXml($values){
        if(!is_array($values) || count($values) <= 0){
            throw new InvalidValueException("数组数据异常！");
        }
        $xml = "<xml>";
        foreach ($values as $key=>$val){
            if (is_numeric($val)){
                $xml.="<".$key.">".$val."</".$key.">";
            }else{
                $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
            }
        }
        $xml.="</xml>";
        return $xml;
    }
    
    /**
     *  
     */
    private function fromXml($xml)
    {
        if(!$xml){
            throw new InvalidValueException("xml数据异常！");
        }
        try
        {
            $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        }
        catch(\Exception $e)
        {
            throw new InvalidValueException("xml数据异常！");
        }
        return $values;
    }
    
    /**
     * 发送数据,post方式 
     */
    private function postXmlCurl($xml, $url, $useCert = false, $second = 30){
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);

        //如果有配置代理这里就设置代理
        if($this->curl_proxy_host != "0.0.0.0" && $this->curl_proxy_port != 0){
            curl_setopt($ch,CURLOPT_PROXY, $this->curl_proxy_host);
            curl_setopt($ch,CURLOPT_PROXYPORT, $this->curl_proxy_port);
        }
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        if($useCert == true && !empty($this->cert_path) && !empty($this->key_path)){
            //设置证书
            //使用证书：cert 与 key 分别属于两个.pem文件
            curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLCERT, $this->cert_path);
            curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLKEY, $this->key_path);
        }
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if($data){
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new InvalidValueException("curl出错，错误码:{$error}");
        }
    }
}
