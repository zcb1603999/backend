$(function () {
     $.datepicker.regional['zh-CN'] = {
        yearRange:'c-20:c',
        closeText: '关闭',  
        prevText: '<上月',  
        nextText: '下月>',  
        currentText: '今天',  
        monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],  
        //monthNamesShort: ['一','二','三','四','五','六','七','八','九','十','十一','十二'],
        monthNamesShort: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'], 
        dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],  
        dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],  
        dayNamesMin: ['日','一','二','三','四','五','六'],  
        weekHeader: '周',  
        dateFormat: 'yy-mm-dd',  
        firstDay: 1,  
        isRTL: false,  
        showMonthAfterYear: true,  
        yearSuffix: '年'};  
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
    
    $('#activity_img_right_area').scrollbox({
        linear: true,
        step: 5,
        delay: 0,
        speed: 200
    });

    $('.footer .wechat').bt('<img src="img/qrcode.jpg" width="70" height="70">',
    {
        width: 74,
        fill: 'white',
        cornerRadius: 0,
        padding: 2,
        strokeWidth: 2,
        strokeStyle: '#6e131a',
        trigger: ['mouseover', 'click'],
        positions: 'top'
    });
    $('.enroll .submit').click(function(){
       var checkPhone=function(phone){
            var plen=phone.length;
            if(plen<7){
                return false;
            }
            var Letters = "1234567890"; 
            var i; 
            var c; 
            for(i=0;i<plen;i++ ){
                c=phone.charAt(i);
                if(Letters.indexOf(c)==-1){
                    return false;
                }
            }
            return true;   
       };
       var phone=$('#parent_phone').val();
       if(!checkPhone(phone)){
           $('#tishi_save .modal-body').html('报名出错，家长电话有错误');
           $('#tishi_save').modal();
           return false;
       }
       var csrfToken = $('input[name="_csrf"]').val();
       $('#enroll_type').val($(this).attr('data'));
       var form=$('#enroll_form');
       $.post({
           url:form.attr('action'),
           data: form.serialize(),
           success:function(data){
               if(data=='success'){
                   $('#tishi_save .modal-body').html('报名成功');
               }else{
                   $('#tishi_save .modal-body').html('报名出错，填写信息有误');
               }
               $('#tishi_save').modal();
           }
       });
    });
    
    $('#baby_birth').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true
    });
});