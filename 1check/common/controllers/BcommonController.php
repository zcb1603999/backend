<?php
/**
 * 后台基类
 */
namespace common\controllers;

use Yii;
use yii\web\Controller;
use common\controllers\UtilTool;
use yii\data\Pagination;

class BcommonController extends Controller{
    use UtilTool;
    
    public $layout = 'backend_layout';
    protected  $pagination = null;
    private $pageSize = 10;


    public function init(){
        UtilTool::initObj();
        $this->pagination = new Pagination();
        $this->pagination->setPageSize($this->pageSize);
    }
    
    /**
     * 将文件或图片存到服务器指定位置
     */
    protected function saveFileToServer($model,$obj_name,$type='image'){
        $f_sha1=sha1_file($model->{$obj_name}->tempName);
        $s_sha1=sha1($f_sha1);
        $t_sha1=sha1($s_sha1);
        $web_path='';
        if($type=='image'){
            $path=Yii::getAlias('@frontend_webroot').'/img/';
            $web_path='/img/';
        }elseif($type=='voice'){
            $path=Yii::getAlias('@frontend_webroot').'/voice/';
            $web_path='/voice/';
        }elseif($type=='excel'){
            $path=Yii::getAlias('@frontend_webroot').'/excel/';
            $web_path='/excel/';
        }
        $sha1_path=substr($t_sha1,1,1).'/'.substr($s_sha1,1,2).'/';
        if(!file_exists($path . $sha1_path)){
            mkdir($path . $sha1_path,0755,true);
        }
        $web_path.=$sha1_path . $f_sha1 . '.' . $model->{$obj_name}->extension;
        $file_fullpath= $path . $sha1_path . $f_sha1 . '.' . $model->{$obj_name}->extension;
        if($model->{$obj_name}->saveAs($file_fullpath)){
            return $web_path;
        }
    }
}
