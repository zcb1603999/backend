<?php
use yii\helpers\Html;

$this->title = '修改用户信息: ';
$this->params['breadcrumbs'][] = ['label' => '用户管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';

?>


<div class="user-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'selected_roles'=>$selected_roles,
        'all_role'=>$all_role
    ]) ?>

</div>