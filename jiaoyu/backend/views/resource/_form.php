<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Resource */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resource-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'resource_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'resource_url')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'parent_id')->hiddenInput(['value' => $parent_id])->label(false) ?>
    <?= $form->field($model, 'level')->hiddenInput(['value' => $level])->label(false) ?>
	<label>选择角色</label>
	<table class="table table-bordered">
		<tbody>
			<tr>
			<?php $count=0; foreach($roles as $role) {?>
				<td><input type="checkbox" name="roles[]" value="<?=$role->id?>" <?=(isset($selected_roles[$role->id])&&$selected_roles[$role->id]?'checked="checked"':'')?>><?=$role->role_name?></input></td>
			<?php 
			$count++;
				if($count%4==0){
					echo "</tr><tr>";	
				}
			} ?>
			</tr>
		</tbody>
	</table>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '创建' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	
    <?php ActiveForm::end(); ?>

</div>
