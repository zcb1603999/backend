<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Resource */

$this->title = $model->resource_name;
$this->params['breadcrumbs'][] = ['label' => 'Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-view">

    <h1>菜单信息</h1>
    <p>
        <?= Html::a('修改', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确认删除改菜单吗?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'resource_name',
            'resource_url',
            'level',
            'addtime',
        ],
    ])?>
    
    <?php if($dataProvider) {
            $operation=[
                'class' => 'yii\grid\ActionColumn',
                'header'=>'操作',
                'template'=>'{view} {update}',
            ];
            if($model->level<2){
                $operation['template']='{view} {update} {createsub}';
                $operation['buttons']=[
                    'createsub'=>function($url,$model,$key){
                                return Html::a('<span class="glyphicon glyphicon-edit"></span>',Yii::$app->urlManager->createUrl(['resource/create','id'=>$key]),[
                                    'title'=>'添加子菜单',
                                ]);
                            }
                ];
            }
    ?>
    <h2>资源子信息</h2>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>'第{page}/{pageCount}页，本页{count}条，共{totalCount}条',
        'columns' => [
            'id',
            'resource_name',
            'resource_url',
            'level',
            'addtime',
            $operation
        ],
    ]) ?>
    <?php } ?>
</div>
