<?php
use frontend\assets\AppAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);
AppAsset::addScript($this,'@web/js/bootstrap.min.js');
AppAsset::addScript($this,'@web/js/jquery-ui.min.js');
AppAsset::addScript($this,'@web/js/jquery.BubblePopup.min.js');
AppAsset::addScript($this,'@web/js/jquery.bt.min.js');
AppAsset::addScript($this,'@web/js/jquery.scrollbox.min.js');
AppAsset::addScript($this,'@web/js/juai.js');
AppAsset::addCss($this, '@web/css/bootstrap.min.css');
AppAsset::addCss($this, '@web/css/jquery-ui.min.css');
AppAsset::addCss($this, '@web/css/juai.css');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
         <!--<meta name="viewport" content=" initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">-->
        <title>巨爱教育</title>
         <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>
        <div class="container navbar-fixed-top juai_top_navbar">
            <div class="row juai-menu">
                <div class="col-xs-2"></div>
                <div class="col-xs-2 logo"><img src="<?=Url::to('img/logo.jpg')?>"></div>	
                <div class="col-xs-1"></div>
                <div class="col-xs-1 name"><a href="#home">首页<br/><span class="name_en">Home</span></a></div>
                <div class="col-xs-1 name"><a href="#aboutus">关于巨爱<br/><span class="name_en">About Us</span></a></div>
                <div class="col-xs-2 name"><a href="#school">劲葛琳克文通校<br/><span class="name_en">Wen Tong School</span></a></div>
                <div class="col-xs-1 name"><a href="#activity">活动集锦<br/><span class="name_en">Activity</span></a></div>
                <div class="col-xs-1 name"><a href="#enroll">我要报名<br/><span class="name_en">Application</span></a></div>
                <div class="col-xs-1"></div>
            </div>		
        </div>
        <div class="jumbotron juai-banner">
            <a name="home"></a> 
            <img src="<?=Url::to('img/banner.jpg')?>" />
        </div>

        <div class="container brief">
            <div class="row">
                <div class="col-xs-12 juai-intro"><a name="aboutus"></a>
                    <div class="title">关于巨爱</div>
                    <div class="sub_title">ABOUT INFINITE LOVE EDUCATION</div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-4 brief_up_left">
                    <div class="brief_title">
                        巨爱简介
                    </div>
                    <div class="brief_content">
                        <p>巨爱教育集团引入了台湾知名学校--劲葛琳克的教育理念、教学经验及教学系统，致力于启动婴幼儿全脑开发的全人教育。 </p>
                        <p>Gymgolin婴幼教育系统，是由旅美教育学博士一储瑄于1987年在台湾成立。劲葛琳克成立30年来，已开办50多所分校，毕业学生逾万人，育英才无数。</p>
                        <p>现在，巨爱取其精华将其引入内地，并推陈出新，在原有基础上打造出一套更加适合婴幼儿的课程体系。</p>
                    </div>
                </div>
                <div class="col-xs-4 brief_up_right">
                    <img src="<?=Url::to('img/brief_img1.jpg')?>">
                </div>
                <div class="col-xs-2"></div>
            </div>
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-4 brief_down_left">
                    <div class="brief_img_sub">
                        <img class="left" src="<?=Url::to('img/brief_img_sub1.jpg')?>">
                        <img class="right" src="<?=Url::to('img/brief_img_sub2.jpg')?>">
                    </div>
                </div>
                <div class="col-xs-4 brief_down_right">
                    <div class="brief_news">
                        <div class="news_title">集团新闻</div>
                        <div class="news_content">
                            <p>2016年12月10日，筹备了近半年的劲葛琳克文 通校盛大开业！</p>
                            <p>学苑的装饰设计从满足婴幼儿的健康及生活起居出发，采用温暖明亮的色调，卡通造型的设计，并选用绿色、安全、可食用的环保材料，为宝宝的学习生活增添了更多乐趣。</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2"></div>
            </div>
        </div>


        <div class="container school">
            <div class="row">
                <div class="col-xs-12 juai_school">
                    <div class="title"><a name="school"></a>劲葛琳克文通校</div>
                    <div class="sub_title">GYMGOLINK EARLY CHILDHOOD EDUCATION CENTER</div>
                </div>
            </div>
            <div class="row school_up_intro">
                <div class="col-xs-2"></div>
                <div class="col-xs-8">
                    <ul class="intro">
                        <li class="text_img"><img src="<?=Url::to('img/school1.jpg')?>" /></li>
                        <li class="text_img">
                            <div class="text_title">智慧阅读小站</div>
                            <div class="text_content">
                                <p>阅读小站以企鹅轮廓作为门的外形，萌萌哒的既视感有木有。既方便教师随时观察了解宝宝们在阅读室内的情况，又能吸引宝宝的目光。 </p>
                                <p>在书架的设计方面，以宝宝能够自行取阅为出发点，培养宝宝自主阅读的能力。</p>
                            </div>
                        </li>
                        <li class="text_img"><img src="<?=Url::to('img/school2.jpg')?>" /></li>
                        <li class="text_img">
                            <div class="text_title">人性化教室</div>
                            <div class="text_content">
                                <p>教室的门采用上下分段式，预防宝宝在进出教室时发生磕碰。 </p>
                                <p>针对不同月龄段的宝宝，我们有不同的教室装修环境和设施。譬如零--六个月大的宝宝，教室特别设有清洗区域，方便宝宝随时处理卫生清洁问题。</p>
                            </div>
                        </li>
                    </ul>
                    <ul class="intro">
                        <li class="text_img" style="margin-bottom: 30px;">
                            <div class="text_title">干净整洁的厨房</div>
                            <div class="text_content">
                                <p>劲葛琳克婴幼学苑文通校区为不同月龄段的宝宝聘请了专业营养师，并由专业餐饮公司现场制作膳食，确保宝宝每天的食材都是当天采购，新鲜可口。</p>
                                <p>厨房还设有留样冰箱，多方面保证食品的新鲜。</p>
                            </div>
                        </li>
                        <li class="text_img"><img src="<?=Url::to('img/school3.jpg')?>" /></li>
                        <li class="text_img">
                            <div class="text_title">多功能活动中心</div>
                            <div class="text_content">
                                <p>这里可能是未来萌宝最喜欢也最常来的地方，所以我们更加用心和仔细。</p>
                                <p>地面采用木质地板，减轻地面对宝宝造成的意外伤害。</p>
                                <p>活动室内，设有儿童游乐器具及专业的体能活动器材，譬如平衡木、单杠器械、球类等。宝宝们可以任性选，开心玩耍。</p>
                            </div>
                        </li>
                        <li class="text_img"><img src="<?=Url::to('img/school4.jpg')?>" /></li>
                    </ul>
                </div>
                <div class="col-xs-1"></div>
            </div>
        </div>

        <div class="container activity" style="background-color: #b01d23;">
            <div class="col-xs-2"><a name="activity"></a></div>
            <div class="col-xs-2">
                <ul class="activity_img_left">
                    <li><img src="<?=Url::to('img/activity1.jpg')?>" /></li>
                    <li><img src="<?=Url::to('img/activity2.jpg')?>" /></li>
                </ul>
            </div>
            <div class="col-xs-3" id="activity_img_right_area">
                <ul class="activity_img_right">
                    <li><img src="<?=Url::to('img/activity3.jpg')?>" /></li>
                    <li><img src="<?=Url::to('img/activity4.jpg')?>" /></li>
                    <li><img src="<?=Url::to('img/activity5.jpg')?>" /></li>
                    <li><img src="<?=Url::to('img/activity6.jpg')?>" /></li>
                    <li><img src="<?=Url::to('img/school4.jpg')?>" /></li>
                </ul>
            </div>
            <div class="col-xs-3  activity_intro">
                <div class="title">活动集锦</div>
                <div class="sub_title sub_title1">Activity</div>
                <div class="sub_title sub_title2">Brocade</div>
                <div class="content">
                    <div class="biaoti">一、体能大比拼：</div>
                    <div class="text">
                        <p>准备：小兔子头饰若干、胡萝卜布偶若干、小桶若干、万象组合一套 </p>
                        <p>主题：小兔子饿了，要去找食物...... </p>
                        <p>介绍：小兔子要去河对岸找胡萝卜，首先要走过独木桥（平衡木）、钻过山洞、跳过障碍物（体能圈）找到胡萝卜后放回，放到自己的小桶里。</p>
                    </div>
                    <div class="biaoti">二、我是小画家：</div>
                    <div class="text">
                        <p>准备：红色、黄色、蓝色无毒安全颜料、画有树干的大白纸、盘子</p>
                        <p>主题：春天来了，树叶新长出来了......</p>
                        <p>介绍：依次教宝宝认识三原色，随后教师示范将树叶在颜料盘里印一下，然后在大白纸上印出不同颜色树叶。</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-2"></div>
        </div>


        <div class="container enroll">
            <div class="row">
                <div class="col-xs-12 title"><a name="enroll"></a>我要报名</div>
            </div>
            <?php $form = ActiveForm::begin([
                'id'=>'enroll_form',
                'action' => Url::to(['home/enroll']),           //此处为请求地址 Url用法查看手册
                'enableAjaxValidation' => true,
                    'validationUrl' => Url::to(['post/validate']),     //数据异步校验
            ]); ?>
            <input type="hidden" name="UserEnroll[enroll_type]" id="enroll_type" value="">
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-2">
                    <div class="form-group">
                        <label for="exampleInputEmail1">宝宝姓名<sup>*</sup></label>
                        <input type="text" class="form-control" id="baby_name" name="UserEnroll[baby_name]">
                    </div>
                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-2">
                    <div class="form-group">
                        <label for="baby_birth">宝宝生日</label>
                        <input type="text" class="form-control" id="baby_birth" name="UserEnroll[baby_birthday]">
                    </div>
                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-2">
                    <div class="form-group">
                        <label for="baby_sex">宝宝性别</label><br/>
                        <label class="radio-inline">
                            <input type="radio" name="UserEnroll[baby_sex]" id="baby_sex" value="1" checked="checked">王子
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="UserEnroll[baby_sex]" id="baby_sex" value="2">公主
                        </label>
                    </div>
                </div>
                <div class="col-xs-1"></div>
            </div>

            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="parent_name">家长姓名</label>
                        <input type="text" class="form-control" id="parent_name" name="UserEnroll[parent_name]">
                    </div>
                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="parent_phone">家长电话</label>
                        <input type="text" class="form-control" id="parent_phone" name="UserEnroll[parent_phone]">
                    </div>
                </div>
                <div class="col-xs-1"></div>
            </div>
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-8">
                    <div class="form-group">
                        <label for="address">家庭住址</label>
                        <input type="text" class="form-control" id="address" name="UserEnroll[address]">
                    </div>
                </div>
                <div class="col-xs-1"></div>
            </div>
            <div class="row submit_button">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <button type="button" data="1" class="btn btn-primary btn-lg submit">预约试听</button>
                </div>
                <div class="col-xs-1"></div>
                <div class="col-xs-3">
                    <button type="button" data="2" class="btn btn-primary btn-lg submit">立即报名</button>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>


        <div class="container footer">
            <div class="container footer_bk">

            </div>
            <div class="container content">
                <div class="row">
                    <div class="col-xs-2"></div>
                    <div class="col-xs-4">
                        <p>电话：021-61305930</p>
                        <p>地址：上海市杨浦区通北路749号文通大厦二楼</p>
                    </div>
                    <div class="col-xs-4" style="text-align: center;">
                        <p>版权所有&copy;劲葛琳克 保留所有权利。</p>
                        <p>沪ICP备17003268号-1</p>
                    </div>
                    <div class="col-xs-1 wechat"><img title="" src="<?=Url::to('img/wechat.png')?>"></div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
        </div>
        
        
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="tishi_save" style="display: none;">
            <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <h4 class="modal-title">提示</h4>
                </div>
                <div class="modal-body">
                </div>
              </div>
            </div>
          </div>
        <?php $this->endBody() ?>
    </body>

</html>
<?php $this->endPage() ?>