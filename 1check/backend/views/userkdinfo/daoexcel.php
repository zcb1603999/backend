<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
AppAsset::addScript($this,'@web/js/fileinput.js');

$this->params['breadcrumbs'][] = '导入excel';
?>
<div class="user-kd-info-create">
    <?php $error=Yii::$app->getSession()->getFlash('error'); 
    if($error){
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>错误</strong>&nbsp;&nbsp;<?=$error?>
    </div>
    <?php } ?>
    
    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?> 

    <?php $form = ActiveForm::begin([
            'options'=>[
                'class'=>'form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
            'fieldConfig'=>[
                'template'=>'{label}<div class="col-lg-7">{input}</div>{error}',
                'labelOptions'=>['class' => 'col-lg-1 control-label']
            ]]); ?>
        <?= $form->field($model, 'file_path',['template'=>'{label}<div class="col-lg-5"><label class="control-label">{input}</label></div>'])->fileInput(['class'=>'file']) ?>
        
        <div class="form-group">
            <div class="col-lg-offset-1  col-lg-3">
                <?= Html::submitButton('导入excel', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    
    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    <?php $this->beginBlock('js_body') ?>  
    $('#yichaexcelfile-file_path').fileinput({
        allowedFileExtensions : ['xls','xlsx'],
        maxFileCount:1,
        minFileCount:1,
        showUpload:false,
    });
    <?php $this->endBlock(); ?>
</script>
<?php $this->registerJs($this->blocks['js_body'],\yii\web\View::POS_END);?>   