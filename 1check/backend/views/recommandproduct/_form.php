<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\assets\AppAsset;

AppAsset::register($this);
AppAsset::addCss($this, '@web/css/fileinput.css');
AppAsset::addScript($this,'@web/js/fileinput.js');
?>

<div class="yicha-recommand-product-form">

    <?php $form = ActiveForm::begin([
            'options'=>[
                'class'=>'form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
            'fieldConfig'=>[
                'template'=>'{label}<div class="col-lg-5">{input}</div>{error}',
                'labelOptions'=>['class' => 'col-lg-1 control-label']
            ]]); ?>

    <?= $form->field($model, 'pname')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'pintro')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'image_url',['template'=>'{label}<div class="col-lg-5"><label class="control-label">{input}</label></div>'])->fileInput(['class'=>'file']) ?>
    <?= $form->field($model, 'is_use',['template'=>'<label class="col-lg-1 control-label" for="yichabanner-orders">启用状态</label><div class="col-lg-2">{label}{input}{error}</div>'])->checkbox() ?>

    <div class="form-group">
        <div class="col-lg-offset-1  col-lg-3">
        <?= Html::submitButton($model->isNewRecord ? '新建' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    <?php $this->beginBlock('js_body') ?>  
    $('#yicharecommandproduct-image_url').fileinput({
        allowedFileExtensions : ['jpg'],
        showUpload:false,
        <?php //if($model->cover_image_url){ ?>
        /*initialPreview:[
            "<img src='"+$('#cover_image_url_preview').val()+"' class='file-preview-image' style='width:220px;'>"
        ]*/
        <?php //} ?>
    });
    <?php $this->endBlock(); ?>
</script>
