<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\YichaBannerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="yicha-banner-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'store_user_id') ?>

    <?= $form->field($model, 'outlink') ?>

    <?= $form->field($model, 'img_url') ?>

    <?= $form->field($model, 'orders') ?>

    <?php // echo $form->field($model, 'is_use') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
