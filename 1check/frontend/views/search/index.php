<?php
use frontend\assets\AppAsset;
use yii\helpers\Url;
AppAsset::register($this);
AppAsset::addScript($this,'@web/js/framework7.min.js');
AppAsset::addScript($this,'@web/js/my-app.js');
AppAsset::addCss($this, '@web/css/framework7.ios.min.css');
AppAsset::addCss($this, '@web/css/framework7.ios.colors.min.css');
AppAsset::addCss($this, '@web/css/framework7-icons.css');
AppAsset::addCss($this, '@web/css/my-app.css');
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no"/>
        <title>一查</title>
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>

        <div class="views">
            <div class="view view-main">

                <div class="navbar slogan_bar">
                    <div class="navbar-inner">
                        <div class="center"><img class="slogan_img" src="<?=Yii::getAlias('@image_domain')?>/img/slogan.jpg"/></div>
                    </div>
                </div>
                <div class="pages">
                    <div class="page-content">
                        <div class="swiper-container yicha_banner">
                            <div class="swiper-pagination"></div>
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img src="<?=Yii::getAlias('@image_domain')?>/img/banner.jpg"/></div>
                                <div class="swiper-slide"><img src="<?=Yii::getAlias('@image_domain')?>/img/banner.jpg"/></div>
                                <div class="swiper-slide"><img src="<?=Yii::getAlias('@image_domain')?>/img/banner.jpg"/></div>
                            </div>
                        </div>

                        <div class="content-block yicha_block_area product">
                            <div class="content-block-inner">
                                <div class="product_image">
                                    <img src="<?=Yii::getAlias('@image_domain')?>/img/store_logo.jpg" />
                                </div>
                                <div class="product_info">
                                    <div class="name">产品名称</div>
                                    <div class="intro">产品介绍一下的内容</div>
                                    <div class="price">价格：10.00元</div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <div class="content-block yicha_block_area yicha_search">
                            <div class="content-block-inner">
                                <form id="search_form" class="ajax-submit" method="post" action="<?=Url::to(['search/kdinfo'])?>">
                                    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
                                    <input type="text" name="info" value="" placeholder="快递单号或手机" />
                                    <button type="submit" id="search_kd" class="search_button">查询订单号</button>
                                </form>
                            </div>
                        </div>
                        <div class="yicha_search_content"></div>

                        <div class="content-block yicha_block_area qrcode">
                            <div class="content-block-inner">
                                <div class="qrcode_image">
                                    <img src="<?=Yii::getAlias('@image_domain')?>/img/qrcode.jpg" />
                                </div>
                                <div class="qrcode_intro">
                                    <p>识别二维码，关注【一查】官方微信</p>
                                    <p>了解最前沿微商资讯</p>
                                </div>
                            </div>
                        </div>

                        <div class="content-block yicha_block_area mini_store">
                            <div class="content-block-inner">
                                <div class="store_image">
                                    <img src="<?=Yii::getAlias('@image_domain')?>/img/store_logo.jpg" />
                                </div>
                                <div class="store_info">
                                    <div class="name">微店名称</div>
                                    <div class="contact">021-6535432</div>
                                </div>
                                <div class="store_enter">
                                    <a href="">进入店铺&gt;&gt;</a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <div class="content-block yicha_block_area recommend">
                            <div class="content-block-inner">
                                <div class="title">本店新品</div>
                                <div class="item_list">
                                    <div class="item left">
                                        <a href=""><img src="<?=Yii::getAlias('@image_domain')?>/img/item_left.jpg" /></a>
                                    </div>
                                    <div class="item right">
                                        <a href=""><img src="<?=Yii::getAlias('@image_domain')?>/img/item_right.jpg" /></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                        <div class="content-block yicha_block_area notice">
                            <div class="content-block-inner">
                                <div class="title">公告:</div>
                                <div class="content">
                                    <p>每日订单17:00更新</p>
                                    <p>新品需要购买可以加掌柜微信:watch4729</p>
                                    <p>诚招代理</p>
                                    <p>1月20号最后发货日期</p>
                                    <p>祝大家新年快乐</p>
                                </div>
                            </div>
                        </div>

                        <div class="content-block footer">
                            <div class="logo">
                                <img src="<?=Yii::getAlias('@image_domain')?>/img/footer_logo.jpg" />
                            </div>
                            <div class="copyright">
                                <p>【一查】平台由掌闻科技提供支持</p>
                                <p>www.1-check.com</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="toolbar toolbar-bottom yicha_toolbar">
                    <div class="toolbar-inner">
                        <a href="#" class="link"><i class="f7-icons">home_fill</i></a>
                        <a href="#" class="link open-login-screen"><i class="f7-icons">person_fill</i></a>
                    </div>
                </div>
            </div>

        </div>
        
        
        <div class="login-screen">
                <div class="view view-login">
                    <div class="navbar slogan_bar">
                        <div class="navbar-inner">
                            <div class="center"><img class="slogan_img" src="<?=Yii::getAlias('@image_domain')?>/img/slogan.jpg"/></div>
                        </div>
                    </div>

                    <div class="pages">
                        <div class="page-content" style="background-color: #fff;">
                            <div class="content-block login_logo">
                                <img src="<?=Yii::getAlias('@image_domain')?>/img/login_log.jpg" />
                            </div>
                            <div class="content-block login_info">
                                <form id="login_form" method="post" class="ajax-submit" action="<?=Url::to(['search/login'])?>">
                                <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
                                <div class="list-block">
                                    <ul>
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i class="icon icon-form-name"><img src="<?=Yii::getAlias('@image_domain')?>/img/login_user.jpg" /></i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input type="number" name="LoginForm[user_phone]" placeholder="手机号">
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-content">
                                                <div class="item-media"><i class="icon icon-form-name"><img src="<?=Yii::getAlias('@image_domain')?>/img/login_passwd.jpg" /></i></div>
                                                <div class="item-inner">
                                                    <div class="item-input">
                                                        <input type="password" name="LoginForm[password]" placeholder="密码">
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <input type="submit" class="button button-big button-fill login_submit" value="登录">
                                </div>
                                </form>
                                <div class="row login_other">
                                    <div class="col-50"><a data-popup=".forget_password" class="open-popup" href="#">忘记密码</a></div>
                                    <div class="col-50"><a data-popup=".user_sign" class="open-popup" href="#">手机号注册</a></div>
                                </div>
                                <div class="row login_close">
                                    <a href="#" class="close-login-screen"><i class="f7-icons">close_round_fill</i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        
        <div class="popup user_sign">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="left"></div>
                    <div class="center">手机号注册</div>
                    <div class="right"></div>
                </div>
            </div>
            <form id="sign_form" class="ajax-submit" method="post" action="<?=Url::to(['search/sign'])?>">
            <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
                <div class="content-block sign_area">
                    <div class="list-block">
                        <ul>
                            <li>
                                <div class="item-content">
                                  <div class="item-media"><i class="icon icon-form-name"><img src="<?=Yii::getAlias('@image_domain')?>/img/login_user.jpg" /></i></div>
                                  <div class="item-inner">
                                    <div class="item-input">
                                        <input type="number" name="SignupForm[user_phone]" placeholder="手机号">
                                    </div>
                                  </div>
                                </div>
                              </li>
                              <li>
                                <div class="item-content">
                                  <div class="item-inner">
                                    <div class="item-input">
                                        <input type="text" name="verify_code" value="">
                                    </div>
                                  </div>
                                    <div class="item-media"><button class="getValidateCode">获取验证码</button></div>
                                </div>
                              </li>
                              <li>
                                <div class="item-content">
                                  <div class="item-media"><i class="icon icon-form-email"><img src="<?=Yii::getAlias('@image_domain')?>/img/login_passwd.jpg" /></i></div>
                                  <div class="item-inner">
                                    <div class="item-input">
                                        <input type="password" name="SignupForm[password]" placeholder="密码">
                                    </div>
                                  </div>
                                </div>
                              </li>
                        </ul>
                        <input type="submit" class="button button-big button-fill sign_submit" value="完成">
                    </div>
                </div>
            </form>
        </div>
        <?php $this->endBody() ?>
    </body>
    
</html>
<?php $this->endPage() ?>