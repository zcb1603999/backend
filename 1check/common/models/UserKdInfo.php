<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_kd_info".
 *
 * @property integer $id
 * @property integer $store_user_id
 * @property string $user_name
 * @property string $phone
 * @property string $province
 * @property string $city
 * @property string $area
 * @property string $address
 * @property string $kd_number
 * @property integer $kd_company_id
 * @property string $product_name
 * @property string $send_user
 * @property string $remark
 * @property string $addtime
 */
class UserKdInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_kd_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_user_id'], 'required'],
            [['store_user_id', 'kd_company_id'], 'integer'],
            [['addtime'], 'safe'],
            [['user_name', 'kd_number', 'send_user'], 'string', 'max' => 128],
            [['phone', 'province', 'city', 'area'], 'string', 'max' => 64],
            [['address', 'product_name', 'remark'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_user_id' => 'Store User ID',
            'user_name' => 'User Name',
            'phone' => 'Phone',
            'province' => 'Province',
            'city' => 'City',
            'area' => 'Area',
            'address' => 'Address',
            'kd_number' => 'Kd Number',
            'kd_company_id' => 'Kd Company ID',
            'product_name' => 'Product Name',
            'send_user' => 'Send User',
            'remark' => 'Remark',
            'addtime' => 'Addtime',
        ];
    }
    
    /**
     * 获取需要订阅的快递数据
     */
    public function getNeedSubscribeData(){
        return $this->find()->where(['is_subscribe'=>0])->asArray()->all();
    }
    
    public function findByKdNumber($num,$store_user_id){
        $kd_data=[];
        $data=$this->find()->where(['kd_number'=>$num,'store_user_id'=>$store_user_id])->asArray()->one();
        $kd_cp_tmp=new KdCompany();
        $kd_cp=$kd_cp_tmp->getAll();
        if(empty($data)){//通过手机号查询
            $tmp_data=$this->find()->where(['phone'=>$num,'store_user_id'=>$store_user_id])->asArray()->all();
            if($tmp_data){
                foreach($tmp_data as $v){
                    $info=json_decode($v['kd_info'],true);
                    if(!empty($info)){
                        krsort($info);
                    }else{
                        continue;
                    }
                    $kd_data[$v['kd_number']]=[
                        'status'=>$v['kd_status'],
                        'info'=>$info,
                        'kd_cp'=>isset($kd_cp[$v['kd_company_id']])?$kd_cp[$v['kd_company_id']]:''
                    ];
                }
            }
        }else{//直接显示快递信息
            $info=json_decode($data['kd_info'],true);
            if(!empty($info)){
                krsort($info);
                $kd_data[$data['kd_number']]=[
                    'status'=>$data['kd_status'],
                    'info'=>$info,
                    'kd_cp'=>isset($kd_cp[$data['kd_company_id']])?$kd_cp[$data['kd_company_id']]:''
                ];
            }
        }
        return $kd_data;
    }
    
    public static function ColNameRule(){
        return [
            '收件人'=>'R',
            '手机号'=>'R',
            '单号'=>'R',
            '快递公司'=>'R',
            '发货人'=>'R',
            '省'=>'O',
            '市'=>'O',
            '区'=>'O',
            '地址'=>'O',
            '产品名称'=>'O',
            '备注'=>'O'
        ];
    }
}
