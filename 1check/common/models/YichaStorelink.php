<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yicha_storelink".
 *
 * @property integer $id
 * @property integer $store_user_id
 * @property string $store_name
 * @property string $phone
 * @property string $logo_img
 * @property string $outlink
 * @property integer $is_use
 */
class YichaStorelink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yicha_storelink';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_user_id', 'is_use'], 'required'],
            [['store_user_id', 'is_use'], 'integer'],
            [['store_name', 'phone', 'logo_img'], 'string', 'max' => 128],
            [['outlink'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_user_id' => 'Store User ID',
            'store_name' => 'Store Name',
            'phone' => 'Phone',
            'logo_img' => 'Logo Img',
            'outlink' => 'Outlink',
            'is_use' => 'Is Use',
        ];
    }
}
