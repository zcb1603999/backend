<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserEnroll */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-enroll-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'baby_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'baby_birthday')->textInput() ?>

    <?= $form->field($model, 'baby_sex')->textInput() ?>

    <?= $form->field($model, 'parent_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'enroll_type')->textInput() ?>

    <?= $form->field($model, 'addtime')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
