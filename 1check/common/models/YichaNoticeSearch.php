<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\YichaNotice;

/**
 * YichaNoticeSearch represents the model behind the search form about `common\models\YichaNotice`.
 */
class YichaNoticeSearch extends YichaNotice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_user_id', 'content', 'orders'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = YichaNotice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_user_id' => $this->store_user_id,
            'content' => $this->content,
            'orders' => $this->orders,
        ]);

        return $dataProvider;
    }
}
