<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserKdInfo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Kd Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-kd-info-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'store_user_id',
            'user_name',
            'phone',
            'province',
            'city',
            'area',
            'address',
            'kd_number',
            'kd_company_id',
            'product_name',
            'send_user',
            'remark',
            'addtime',
            'kd_status',
            'kd_info:ntext',
            'is_subscribe',
        ],
    ]) ?>

</div>
