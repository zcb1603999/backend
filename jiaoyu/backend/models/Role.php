<?php

namespace backend\models;

use Yii;
use backend\models\User;
use backend\models\Resource;

/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string $role_name
 * @property string $addtime
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_name'], 'required'],
            [['id'], 'integer'],
            [['addtime'], 'safe'],
            ['addtime', 'default','value'=>date('Y-m-d H:i:s')],
            [['role_name'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '编号',
            'role_name' => '角色名称',
            'addtime' => '添加时间'
        ];
    }
	
    public function getUsers(){
        return $this->hasMany(User::className(),['id'=>'user_id'])->viaTable('user2role',['role_id'=>'id']);
    }

    public function getResources(){
        return $this->hasMany(Resource::className(),['id'=>'resource_id'])->viaTable('role2resource',['role_id'=>'id']);
    }

    public function createRelationResource($id_arr,$relation_data){
        if(isset($id_arr['role'])){
            Yii::$app->db->createCommand()->delete('role2resource', [
                'role_id' => $id_arr['role'],
            ])->execute();
            if(empty($relation_data)){
                return null;
            }
            foreach($relation_data as $resource_id){
                Yii::$app->db->createCommand()->insert('role2resource', [
                    'role_id' => $id_arr['role'],
                    'resource_id' => $resource_id,
                ])->execute();
            }
        }elseif(isset($id_arr['resource'])){
            Yii::$app->db->createCommand()->delete('role2resource', [
                'resource_id' => $id_arr['resource'],
            ])->execute();
            if(empty($relation_data)){
                return null;
            }
            foreach($relation_data as $role_id){
                Yii::$app->db->createCommand()->insert('role2resource', [
                    'role_id' => $role_id,
                    'resource_id' => $id_arr['resource'],
                ])->execute();
            }
        }
    }
}
