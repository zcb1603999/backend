<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
$this->params['breadcrumbs'][] = ['label' => '公告列表', 'url' => ['index']];
$this->params['breadcrumbs'][] = '修改公告';
?>
<div class="yicha-notice-update">

    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?> 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
