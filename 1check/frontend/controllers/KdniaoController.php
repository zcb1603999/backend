<?php

namespace frontend\controllers;

use Yii;
use common\controllers\FcommonController;
use yii\filters\VerbFilter;
use common\models\UserKdInfo;
use common\models\KdNiaoAccept;


class KdniaoController extends FcommonController{
    public $enableCsrfValidation = false;
    
    /*public function behaviors(){
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'accept' => ['post'],
                ],
            ],
        ];
    }*/
    
    public function actionTest(){
        $model=new UserKdInfo();
        $data=$model->getNeedSubscribeData();
    }
    
    public function actionAccept(){
        $post=Yii::$app->request->post();
        $error=false;
        file_put_contents("/home/wwwroot/1check/frontend/web/log.txt", var_export($post,true),FILE_APPEND);
        $return_data=[
            'EBusinessID'=>Yii::$app->params['kdniao.EBusinessID'],
            'UpdateTime'=>date('Y-m-d H:i:s'),
            'Success'=>true
        ];
        
        if(!empty($post)){
            //$sign=$post['DataSign'];
            if(!empty($post['RequestData'])){
                $data=json_decode($post['RequestData'],true);
                if(isset($data['Data'])&&!empty($data['Data'])){
                    foreach($data['Data'] as $v){
                        if($v['EBusinessID']==Yii::$app->params['kdniao.EBusinessID']){
                            $v['OrderCode']=1;//测试用
                            if(isset($v['OrderCode'])&&!empty($v['OrderCode'])){
                                if($post['RequestType']=='101'){
                                    $model=UserKdInfo::findOne($v['OrderCode']);
                                    if($model){
                                        $model->kd_info=json_encode($v['Traces']);
                                        $model->kd_status=$v['State'];
                                        $model->save();
                                    }
                                }
                                $accept_model=KdNiaoAccept::find()->where(['kd_info_id'=>$v['OrderCode'],'request_type'=>$post['RequestType']])->one();
                                if(!$accept_model){
                                    $accept_model=new KdNiaoAccept();
                                    $accept_model->kd_info_id=$v['OrderCode'];
                                    $accept_model->request_type=$post['RequestType'];
                                }
                                if($post['RequestType']=='101'){
                                    $accept_model->request_data=json_encode($v['Traces']);
                                }elseif($post['RequestType']=='107'){
                                    $accept_model->request_data=json_encode($v);
                                }
                                $accept_model->save();
                            }
                        }
                    }
                }
            }
        }else{
            $error=true;
        }
        if($error){
            $return_data['Success']=false;
        }
        echo json_encode($return_data);
    }
}
