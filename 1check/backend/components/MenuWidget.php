<?php
namespace backend\components;

use Yii;
use yii\base\Widget;
use backend\models\Resource;

/**
 * 后台菜单显示组件
 */
class MenuWidget extends Widget{
	
	public function run(){
            //if (!Yii::$app->user->isGuest) {
            //}else{
        	$model=new Resource();
                $result=$model->getUserResource(3);
                $menu=Resource::toArrayMenu($result);
                return $this->render('_menu',['menu'=>$menu]);
        	//Yii::$app->user->id;
            //}
	}
}
