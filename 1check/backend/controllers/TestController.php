<?php

namespace backend\controllers;

use Yii;
use common\controllers\BcommonController;
use backend\models\Test;

class TestController extends BcommonController
{
	
    public function actionIndex()
    {
        $model=new Test();
        return $this->render('index',[
            'model'=>$model
        ]);
    }
    
}
