/*
Navicat MySQL Data Transfer

Source Server         : 1111111111111
Source Server Version : 50508
Source Host           : localhost:3306
Source Database       : auth

Target Server Type    : MYSQL
Target Server Version : 50508
File Encoding         : 65001

Date: 2015-08-29 18:12:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_admin`
-- ----------------------------
DROP TABLE IF EXISTS `db_admin`;
CREATE TABLE `db_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `account` varchar(32) DEFAULT NULL COMMENT '管理员账号',
  `password` varchar(36) DEFAULT NULL COMMENT '管理员密码',
  `mobile` varchar(11) DEFAULT NULL COMMENT '手机号',
  `login_time` int(11) DEFAULT NULL COMMENT '最后登录时间',
  `login_ip` varchar(15) DEFAULT NULL COMMENT '最后登录IP',
  `login_count` mediumint(8) NOT NULL COMMENT '登录次数',
  `email` varchar(40) DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '账户状态，禁用为0   启用为1',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_admin
-- ----------------------------
INSERT INTO `db_admin` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '1440842124', '127.0.0.1', '107', '', '1', '1437979578');
INSERT INTO `db_admin` VALUES ('4', 'guest', '21232f297a57a5a743894a0e4a801fc3', '345676767', '1440159118', '127.0.0.1', '17', '1235656', '1', '1439434685');
INSERT INTO `db_admin` VALUES ('30', '12345', '202cb962ac59075b964b07152d234b70', null, '1439914175', '127.0.0.1', '3', null, '1', '1439910471');
INSERT INTO `db_admin` VALUES ('31', 'zhangsan', '4e7bdb88640b376ac6646b8f1ecfb558', null, null, null, '0', null, '1', '1440840516');
INSERT INTO `db_admin` VALUES ('32', 'admin1', 'c4ca4238a0b923820dcc509a6f75849b', '', '1440842147', '127.0.0.1', '2', '', '1', '1440842077');

-- ----------------------------
-- Table structure for `db_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `db_auth_group`;
CREATE TABLE `db_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_auth_group
-- ----------------------------
INSERT INTO `db_auth_group` VALUES ('26', '游客组', '1', '');
INSERT INTO `db_auth_group` VALUES ('28', '超级管理组', '1', '85,86,87');

-- ----------------------------
-- Table structure for `db_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `db_auth_group_access`;
CREATE TABLE `db_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_auth_group_access
-- ----------------------------
INSERT INTO `db_auth_group_access` VALUES ('1', '28');
INSERT INTO `db_auth_group_access` VALUES ('4', '26');
INSERT INTO `db_auth_group_access` VALUES ('30', '26');
INSERT INTO `db_auth_group_access` VALUES ('31', '26');
INSERT INTO `db_auth_group_access` VALUES ('32', '28');

-- ----------------------------
-- Table structure for `db_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `db_auth_rule`;
CREATE TABLE `db_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `pid` smallint(5) NOT NULL COMMENT '父级ID',
  `sort` smallint(5) NOT NULL COMMENT '排序',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_auth_rule
-- ----------------------------
INSERT INTO `db_auth_rule` VALUES ('85', 'Shops/index', '商铺管理', '1', '1', '', '0', '0', '1439436928');
INSERT INTO `db_auth_rule` VALUES ('86', 'Shops/shops_list', '商铺列表', '1', '1', '', '85', '0', '1439436987');
INSERT INTO `db_auth_rule` VALUES ('87', 'Shops/shops_add', '添加商铺', '1', '1', '', '85', '0', '1439449093');

-- ----------------------------
-- Table structure for `db_shops`
-- ----------------------------
DROP TABLE IF EXISTS `db_shops`;
CREATE TABLE `db_shops` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商户ID',
  `shops` varchar(25) DEFAULT NULL COMMENT '商户账号',
  `password` varchar(32) DEFAULT NULL COMMENT '商户密码',
  `create_time` int(11) DEFAULT NULL,
  `m_nick` varchar(30) DEFAULT NULL COMMENT '商户昵称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_shops
-- ----------------------------

-- ----------------------------
-- Table structure for `db_user`
-- ----------------------------
DROP TABLE IF EXISTS `db_user`;
CREATE TABLE `db_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '客户ID',
  `username` varchar(25) DEFAULT NULL COMMENT '客户用户名',
  `password` varchar(32) DEFAULT NULL COMMENT '客户密码',
  `nickname` varchar(30) DEFAULT NULL COMMENT '商户昵称',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_user
-- ----------------------------
