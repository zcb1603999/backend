<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\components\MenuselectorWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\Role */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-view">
    <p>
        <?= Html::a('修改', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'role_name',
            'addtime',
        ],
    ]) ?>
    
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <label>已关联菜单</label>
                <?=MenuselectorWidget::widget(['selected_resources'=>$resources])?>
            </div>
            <div class="col-md-6">
                <label>已关联用户</label>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <?php $jishu=0;foreach($users as $u){ ?>
                            <td>
                                <?=$u->username?>
                            </td>
                            <?php $jishu++;
                            echo $jishu%2==0?'</tr>':'';
                            } 
                            echo $jishu%2!=0?'</tr>':'';
                            ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    

</div>
