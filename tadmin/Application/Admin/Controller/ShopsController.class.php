<?php
/*
 * @thinkphp3.2.2  auth认证
 * @wamp2.1a  php5.3.3  mysql5.5.8
 * @Created on 2015/08/18
 * @Author  夏日不热    757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;

//商铺管理
class ShopsController extends AuthController {
	//待审核商铺
    public function wait_check(){
    	
    }
 
    //商铺列表(全部)
    public function shops_list(){
    	$this->display();
    	
    }

    //添加商铺
    public function shops_add(){
        $this->display();
    }
    
}




