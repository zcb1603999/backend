<?php
namespace common\models;

use \yii\base\Model;
use common\controllers\UtilTool;

class StoreSearch extends Model{
    use UtilTool;
    public function init(){
        echo "init";
        UtilTool::initObj();
    }
    
    public function getTagAggs($indexname,$type){
        $params=[
            'index' => $indexname,
            'type' => $type,
            'body'=>[
                'size'=>0,
                'aggs'=>[
                    'tag_count'=>[
                        'terms'=>[
                            'field'=>'tag.id',
                        ]
                    ]
                ]
            ]
        ];
        print_r(UtilTool::$elasticObj->search($params));
    }
    
}
