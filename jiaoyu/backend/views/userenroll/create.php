<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserEnroll */

$this->title = 'Create User Enroll';
$this->params['breadcrumbs'][] = ['label' => 'User Enrolls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-enroll-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
