<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_wechat".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $openid
 * @property integer $user_name
 * @property string $access_token
 * @property string $refresh_access_token
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserWechat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_wechat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'openid', 'user_name', 'access_token', 'refresh_access_token', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'openid', 'user_name', 'created_at', 'updated_at'], 'integer'],
            [['access_token', 'refresh_access_token'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'openid' => 'Openid',
            'user_name' => '用户名',
            'access_token' => 'Access Token',
            'refresh_access_token' => 'Refresh Access Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
