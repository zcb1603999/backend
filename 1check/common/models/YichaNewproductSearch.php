<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\YichaNewproduct;

/**
 * YichaNewproductSearch represents the model behind the search form about `common\models\YichaNewproduct`.
 */
class YichaNewproductSearch extends YichaNewproduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_user_id', 'orders', 'is_use'], 'integer'],
            [['outlink', 'img_url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = YichaNewproduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_user_id' => $this->store_user_id,
            'orders' => $this->orders,
            'is_use' => $this->is_use,
        ]);

        $query->andFilterWhere(['like', 'outlink', $this->outlink])
            ->andFilterWhere(['like', 'img_url', $this->img_url]);

        return $dataProvider;
    }
}
