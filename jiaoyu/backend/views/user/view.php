<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;


$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => '用户', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="resource-view">
    <h1>用户信息</h1>
    <p>
        <?= Html::a('修改', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确认删除改菜单吗?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'telphone',
            'email',
            [
                'attribute'=>'created_at',
                'format'=>['date','php:Y-m-d H:i:s']
            ],
            [
                'attribute'=>'updated_at',
                'format'=>['date','php:Y-m-d H:i:s']
            ]
        ],
    ])?>
    
    <label>已拥有角色</label>
    <table class="table table-bordered">
        <tbody>
            <tr>
                <?php $jishu=0;foreach($roles as $r){ ?>
                <td>
                    <?=$r?>
                </td>
                <?php $jishu++;
                echo $jishu%4==0?'</tr>':'';
                } 
                echo $jishu%4!=0?'</tr>':'';
                ?>
        </tbody>
    </table>
</div>