<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'wxpay/wxpay-sdk' => 
  array (
    'name' => 'wxpay/wxpay-sdk',
    'version' => '1.0.0',
    'alias' => 
    array (
      '@wxpay/wxpay/sdk' => $vendorDir . '/wxpay/wxpay-sdk',
    ),
  ),
  'elasticsearch' => 
  array (
    'name' => 'elasticsearch',
    'version' => '2.4.0',
    'alias' => 
    array (
      '@elasticsearch' => $vendorDir . '/elasticsearch/elasticsearch/src/Elasticsearch',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'crazydb/yii2-ueditor' => 
  array (
    'name' => 'crazydb/yii2-ueditor',
    'version' => '1.5.0.0',
    'alias' => 
    array (
      '@crazydb/ueditor' => $vendorDir . '/crazydb/yii2-ueditor',
    ),
  ),
  'callmez/yii2-wechat-sdk' => 
  array (
    'name' => 'callmez/yii2-wechat-sdk',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@callmez/wechat/sdk' => $vendorDir . '/callmez/yii2-wechat-sdk',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.7.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-social' => 
  array (
    'name' => 'kartik-v/yii2-social',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/social' => $vendorDir . '/kartik-v/yii2-social',
    ),
  ),
  'moonlandsoft/yii2-phpexcel' => 
  array (
    'name' => 'moonlandsoft/yii2-phpexcel',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@moonland/phpexcel' => $vendorDir . '/moonlandsoft/yii2-phpexcel',
    ),
  ),
);
