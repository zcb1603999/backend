<?php
namespace frontend\controllers;

use Yii;
use common\controllers\FcommonController;
use common\models\YichaBanner;
use common\models\YichaNewproduct;
use common\models\YichaNotice;
use common\models\YichaRecommandProduct;
use common\models\User;
use common\models\UserKdInfo;
use common\models\LoginForm;
use frontend\models\SignupForm;

class SearchController extends  FcommonController{
    public $layout=false;
    
    public function actionIndex(){
        if(!$user_id=$this->parseDoaminId()){
            $user_id=0;
        }
        $idata=$this->getIndexData($user_id);
        return $this->render('index',[
            'idata'=>$idata
        ]);
    }
    
    public function actionKdinfo(){
        $post=Yii::$app->request->post();
        $data=[];
        if($post['info']){
            $info=trim($post['info']);
            if($user_id=$this->parseDoaminId()){
                $kdinfo=new UserKdInfo();
                $data=$kdinfo->findByKdNumber($info,$user_id);
            }
        }
        return $this->render('kdinfo',[
            'data'=>$data
        ]);
    }
    
    /**
     * 解析出当前访问的二级域名,返回注册商的用户id
     */
    private function parseDoaminId(){
        $host=Yii::$app->request->hostName;
        //保留二级域名名称
        $protect_domain=['www','my','admin'];
        $tmp=explode('.',$host);
        $sub_domain=$tmp[0];
        if($sub_domain){
            if(in_array($sub_domain,$protect_domain)){
                return false;
            }else{
                $user=User::findByDomain($sub_domain);
                if($user){
                    return $user->id;
                }
            }
        }
        return false;
    }
    
    /**
     * 获取首页需要的各种推荐数据和其他信息
     */
    private function getIndexData($store_user_id=0){
        $banner_data=[];
        $newproduct_data=[];
        $notice_data=[];
        $recomproduct_data=[];
        $init_data=[
            'banner'=>[],
            'newproduct'=>[],
            'notice'=>[],
            'recomproduct'=>[]
        ];
        if($store_user_id){
            $banner=new YichaBanner();
            $init_data['banner']=$banner->getNowData($store_user_id);
            $newproduct=new YichaNewproduct();
            $init_data['newproduct']=$newproduct->getNowData($store_user_id);
            $notice=new YichaNotice();
            $init_data['notice']=$notice->getNowData($store_user_id);
            $recomproduct=new YichaRecommandProduct();
            $init_data['recomproduct']=$recomproduct->getNowData($store_user_id);
        }
        return $init_data;
    }
    
    public function actionLogin(){
        $output=[
            'status'=>'',
            'reason'=>'',
        ];
        if (!Yii::$app->user->isGuest) {
            $output['status']='failer';
            $output['reason']='已登录过';
        }else{
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $output['status']='success';
            } else {
                $output['status']='failer';
                $output['reason']='手机或密码错误';
            }
        }
        echo json_encode($output);
    }
    
    public function actionLogout(){
        Yii::$app->user->logout();
        $output=[
            'status'=>'success',
            'reason'=>'',
        ];
        echo json_encode($output);
    }
    
    public function actionSign(){
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $cache_code=$this->getverifycode();
            $cache_code=1234;
            if(Yii::$app->request->post('verify_code')==$cache_code){
                if ($user = $model->signup()) {
                    
                    /*if (Yii::$app->getUser()->login($user)) {
                        //return $this->goHome();
                    }*/
                }else{
                    echo "error";
                }
            }
        }else{
            echo "error2";
        }
        die;
    }
    
    public function actionSendverifycode(){
        $output=[
            'status'=>'success',
            'reason'=>''
        ];
        $cache_code=$this->getverifycode();
        if(!empty($cache_code)){
            $output=[
                'status'=>'failer',
                'reason'=>'验证码已发送过'
            ];
            die;
        }
        
        $mobile = Yii::$app->request->post('mobile');
        $mobile=trim($mobile);
        $code = mt_rand(1024,9800);//生成4位数的验证码
        if($this->sendCodeToSms($code,$mobile)===true){
            if(!Yii::$app->session->isActive){
                Yii::$app->session->open();
            }
            Yii::$app->session->set('login_sms_code',$code);
            Yii::$app->session->set('login_sms_time',time());
            echo json_encode($output);
        }else{
            $output=[
                'status'=>'failer',
                'reason'=>'验证码发送短信失败'
            ];
            echo json_encode($output);
        }
    }
    
    private function getverifycode(){
        if(!Yii::$app->session->isActive){
            Yii::$app->session->open();
        }
        //取得验证码和短信发送时间session
        $signup_sms_code = intval(Yii::$app->session->get('login_sms_code'));
        if(empty($signup_sms_code)||$signup_sms_code==0){
            return '';
        }
        $signup_sms_time = Yii::$app->session->get('login_sms_time');
        if(time()-$signup_sms_time < 600){
            return $signup_sms_code;
        }else{
            return '';
        }
    }
    
    /**
     * 验证码发送短信
     */
    private function sendCodeToSms($code,$phone){
        file_get_contents(Yii::$app->request->hostInfo.'ali-sms-send/action.php?verify_code='.$code.'&phone='.$phone);
        return true;
    }
}
