<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\User;
/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form ActiveForm */
?>
<div class="user-sign">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model_user, 'username') ?>
        <?= $form->field($model_user, 'password')->passwordInput() ?>
        <?= $form->field($model_user, 'email') ?>
        <?= $form->field($model_user, 'telphone') ?>
    	
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- user-sign -->
