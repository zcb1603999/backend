<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->input('email') ?>
    <?= $form->field($model, 'telphone')->textInput() ?>
    <label>选择角色</label>
    <table class="table table-bordered">
        <tbody>
        <tr>
        <?php $jishu=0;foreach ($all_role as $a){ ?>
            <td><input type="checkbox" name="roles[]" value="<?=$a['id']?>" <?=(isset($selected_roles[$a['id']])&&!empty($selected_roles[$a['id']]))?'checked="checked"':''?> /><?=$a['role_name']?></td>
        <?php $jishu++;
            if($jishu%4==0){
                echo "</tr>";
            }
        } ?>
        </tbody>
    </table>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '创建' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>