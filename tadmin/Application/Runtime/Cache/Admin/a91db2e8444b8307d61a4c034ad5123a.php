<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=">
<title>后台管理系统</title>
<link rel="stylesheet" href="/auth/Public/Admin/css/css.css">
<script src="/auth/Public/Admin/js/jquery-1.9.1.js"></script>
<script src="/auth/Public/Common/Layer-1.9.3/layer.js"></script>
<script>
function check(){
	var title = $('#title').val();
	var name = $('#name').val();
	if(title == ''){
		layer.tips('请输入名称', '#title',{tips:1,time: 10000});
		return false;
	}
	if(name == ''){
		layer.tips('请输入（控制器/方法）', '#name',{tips:1,time: 10000});
		return false;
	}
	if(name.indexOf('/') <=0 ){
		layer.tips('格式必须为（控制器/方法）', '#name',{tips:[1, '#78BA32'],time: 10000});
		return false;
	}
	return true;
}
</script>
<script>
function rule_edit(){
	parent.layer.open({
		type: 2,
		closeBtn: 2,
		area: ['470px', '350px'],
		title: '编辑权限',
		content: '../Admin/rule_edit.html'
	});	
}
</script>
</head>
<body>


<div class="nav">    
    <div class="nav_title">
    	<h2><img class="nav_img" src="/auth/Public/Admin/img/tab.gif" /><a class="nav_a" href="#">内容管理</a></h2>
    </div>
</div>

<form name="myform" action="/auth/index.php/Admin/Admin/auth_rule.html" method="post" onsubmit="return check(this)">
<div class="table_top">
菜单状态：<select name="status">
	<option value="1">显示</option>
    <option value="0">不显示</option>
</select>

父级：<select name="pid">
  		<option value="">--------- 顶级 ---------</option>
    <?php if(is_array($data)): foreach($data as $key=>$vo): ?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["title"]); ?></option><?php endforeach; endif; ?>
  </select>
名称：<input type="text" name="title" id="title" class="input" />
控制器/方法：<input type="text" name="name" id="name" class="input" />
<button type="submit" id="submit" class="button">确 定</button>

（ 为防止误操作，请在数据库编辑/删除操作 ）
</div>
</form>

<div class="list">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="list_table">
      <thead>
	    <tr>
	      <td width="8%"><div align="center">ID</div></td>
	      <td width="21%"><div align="center">名称</div></td>
	      <td width="22%"><div align="center">控制器/方法</div></td>
	      <td width="20%"><div align="center">菜单状态</div></td>
	      <td width="29%"><div align="center">创建时间</div>	        <div align="center"></div>	        <div align="center"></div></td>
        </tr>
        </thead>
        <tbody>
        <?php if(is_array($data)): foreach($data as $key=>$vo): ?><tr>
	      <td height="45"><div align="center"><?php echo ($vo["id"]); ?></div></td>
	      <td><div style="padding-left:50px;"><?php echo ($vo["title"]); ?></div>
          </td>
	      <td><div style="padding-left:50px;"><?php echo ($vo["name"]); ?></div></td>
	      <td><div align="center"><?php if( $vo["status"] == 1 ): ?>显示<?php else: ?><span style="color:#F00">不显示</span><?php endif; ?></div></td>
	      <td><div align="center"><?php echo (date("Y-m-d H:i:s",$vo["create_time"])); ?></div></td>
	      </tr>
          	  <?php if(is_array($vo['sub'])): foreach($vo['sub'] as $key=>$sub): ?><tr>
                      <td height="45"><div align="center"><?php echo ($sub["id"]); ?></div></td>
                      <td><div style="padding-left:50px;">&nbsp;&nbsp;&nbsp;&nbsp;┠─&nbsp;&nbsp;<?php echo ($sub["title"]); ?></div></td>
                      <td><div style="padding-left:100px;"><?php echo ($sub["name"]); ?></div></td>
                      <td><div align="center"><?php if( $sub["status"] == 1 ): ?>显示<?php else: ?><span style="color:#C00">不显示</span><?php endif; ?></div></td>
                      <td><div align="center"><?php echo (date("Y-m-d H:i:s",$sub["create_time"])); ?></div></td>
                  </tr><?php endforeach; endif; endforeach; endif; ?>
        </tbody>
  </table>
</div>

<!-- 分页 -->
<div class="page">
  <div align="center"><?php echo ($page); ?> </div>
</div>


</body>
</html>