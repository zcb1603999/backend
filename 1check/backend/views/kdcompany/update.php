<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KdCompany */

$this->title = 'Update Kd Company: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kd Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kd-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
