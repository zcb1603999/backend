<?php
namespace common\controllers;

use Yii;
use yii\console\Controller;
use common\controllers\UtilTool;


class CcommonController extends Controller{
    
    use UtilTool;
    
    public function init(){
        UtilTool::initObj();
    }
    
    protected function Createindex($index_name){
        $params=['index'=>$index_name];
        $return=UtilTool::$elasticObj->indices()->create($params);
        print_r($return);
    }
    
    protected function Deleteindex($index_name){
        $params=['index'=>$index_name];
        UtilTool::$elasticObj->indices()->delete($params);
    }


    public function PutAliasIndex($index_name,$aliasname){
        $params=[
            'index'=>$index_name,
            'name'=>$aliasname
        ];
        $return=UtilTool::$elasticObj->indices()->putAlias($params);
        print_r($return);
    }
    
    public function PutMapping($index_name,$type,$mapping){
        $params=[
            'index'=>$index_name,
            'type'=>$type,
            'body'=>$mapping
        ];
        //print_r($params);
        $return=UtilTool::$elasticObj->indices()->putMapping($params);
        //print_r($return);
    }
    
    public function GetMapping($index_name,$type){
        $params=[
            'index' => $index_name,
            'type' => $type
        ];
        $return = UtilTool::$elasticObj->indices()->getMapping($params);
        print_r($return);
    }
    
    public function PutData($index_name,$type,&$data){
        $offset=0;
        $params = ['body' => []];
        foreach($data as $v){
            $params['body'][]=[
                'index'=>[
                    '_index' => $index_name,
                    '_type' => $type,
                    '_id'=>$v['id']
                ]
            ];
            $params['body'][]=$v;
            if($offset%200==0){
                $response = UtilTool::$elasticObj->bulk($params);
                $params = ['body' => []];
                unset($response);
            }
            $offset++;
        }
        if(!empty($params['body'])){
            $response =UtilTool::$elasticObj->bulk($params);
            unset($response);
        }
    }
}