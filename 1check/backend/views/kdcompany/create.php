<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\KdCompany */

$this->title = 'Create Kd Company';
$this->params['breadcrumbs'][] = ['label' => 'Kd Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kd-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
