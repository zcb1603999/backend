<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use common\models\YichaRecommandProduct;

$this->params['breadcrumbs'][] = '推荐产品列表';
?>
<div class="yicha-recommand-product-index">
    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?> 
    <p>
        <?= Html::a('新建推荐产品', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label'=>'图片',
                'format'=>[
                    'image',
                    [
                        'width'=>'170',
                        'height'=>'120'
                    ]
                ],
                'value' => function ($model) { 
                    return Yii::getAlias('@image_domain').$model->img_url; 
                }
            ],
            'pname',
            'pintro',
            'price',
            [
                'attribute'=>'is_use',
                'value'=>function($model){
                    return YichaRecommandProduct::showUse($model->is_use);
                },
                'filter'=>YichaRecommandProduct::showUse()
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
