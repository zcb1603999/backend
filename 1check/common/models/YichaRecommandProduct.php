<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yicha_recommand_product".
 *
 * @property integer $id
 * @property string $pname
 * @property string $pintro
 * @property string $price
 * @property integer $image_url
 * @property integer $is_use
 */
class YichaRecommandProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yicha_recommand_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pname', 'pintro', 'price', 'image_url', 'is_use'], 'required'],
            [['price'], 'number'],
            [['image_url', 'is_use'], 'integer'],
            [['pname'], 'string', 'max' => 128],
            [['pintro'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pname' => '产品名称',
            'pintro' => '产品介绍',
            'price' => '价格',
            'image_url' => '图片',
            'is_use' => '启用',
        ];
    }
    
    /**
     * 获取当前页面要用的最新数据
     */
    public function getNowData($store_user_id){
        return $this->find()->where(['store_user_id'=>$store_user_id,'is_use'=>1])->limit(2)->asArray()->all();
    }
    
    public static function showUse($val=null){
        $use_status=[
            0=>'未使用',
            1=>'使用中'
        ];
        if($val!==null){
            return array_key_exists($val,$use_status)?$use_status[$val]:$use_status[0];
        }else{
            return $use_status;
        }
    }
}
