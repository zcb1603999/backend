<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Resource */
$this->title = '创建子菜单';
if($parent_id){
    $this->title = '创建子菜单';
}
$this->params['breadcrumbs'][] = ['label' => 'Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'roles'=>$roles,
        'parent_id'=>$parent_id,
        'level'=>$level
    ]) ?>

</div>
