<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'elastic'=>[
            'class' => 'elasticsearch\ClientBuilder',
        ],
    ],
    'aliases'=>[
        'frontend_webroot'=>'@frontend/web',
        'backend_webroot'=>'@backend/web',
        /*'frontend_domain'=>'http://localhost/yichapj/frontend/web',
        'backend_domain'=>'http://localhost/yichapj/backend/web',
        'image_domain'=>'http://localhost/yichapj/frontend/web',
        'file_domain'=>'http://localhost/yichapj/frontend/web',*/
        'frontend_domain'=>'http://www.1-check.com',
        'backend_domain'=>'http://admin.1-check.com',
        'image_domain'=>'http://image.1-check.com',
        'file_domain'=>'http://image.1-check.com',
        
    ]
];
