<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\YichaRecommandProduct;

/**
 * YichaRecommandProductSearch represents the model behind the search form about `common\models\YichaRecommandProduct`.
 */
class YichaRecommandProductSearch extends YichaRecommandProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'image_url', 'is_use'], 'integer'],
            [['pname', 'pintro'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = YichaRecommandProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'image_url' => $this->image_url,
            'is_use' => $this->is_use,
        ]);

        $query->andFilterWhere(['like', 'pname', $this->pname])
            ->andFilterWhere(['like', 'pintro', $this->pintro]);

        return $dataProvider;
    }
}
