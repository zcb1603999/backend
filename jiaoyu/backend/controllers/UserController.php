<?php

namespace backend\controllers;

use Yii;
use common\controllers\BcommonController;
use backend\models\SignupForm;
use backend\models\Role;
use backend\models\User;

class UserController extends BcommonController {

    public function actionSignup() {
        $model_user = new SignupForm();
        if ($model_user->load(Yii::$app->request->post())) {
            if ($user = $model_user->signup()) {
                //if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                //}
            }
        }
        return $this->render('signup', ['model_user' => $model_user]);
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionView($id){
        $model=$this->findModel($id);
        $roles=[];
        foreach($model->roles as $r){
            $roles[$r->id]=$r->role_name;
        }
        return $this->render('view', [
            'model' => $model,
            'roles'=>$roles
        ]);
    }
    
    public function actionUpdate($id){
        $model=$this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if (!$model->validate()) {
                return null;
            }
            $user_model=new User();
            if($model->save()){
                $user_model->createRelationRole(['user'=>$model->id],Yii::$app->request->post('roles'));
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }else{
            $role_model=new Role();
            $all_role=$role_model->find()->asArray()->all();
            $roles_obj=$model->roles;
            $selected_roles=[];
            foreach($roles_obj as $r){
                $selected_roles[$r->id]=$r->id;
            }
            return $this->render('update', [
                'model' => $this->findModel($id),
                'selected_roles'=>$selected_roles,
                'all_role'=>$all_role
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    

    public function actionTest() {
        $model = new \backend\models\Resource();
        $result = $model->getUserResource(3);
        $menu = [];
        usort($result, [$this, 'sortmenu']);
        print_r($result);
        foreach ($result as $r) {
            if ($r['parent_id'] == 0) {
                $menu[$r['id']] = $r;
            } elseif (isset($menu[$r['parent_id']]) && !empty($menu[$r['parent_id']])) {
                $menu[$r['parent_id']]['submenu'][] = $r;
            }
        }
        //$model_role->link('users',$user);
        /* foreach($roles as $role){
          echo $role->id,'--';
          echo $role->role_name;
          echo "<br/>";
          } */

        echo "over";
    }

    private function sortmenu($a, $b) {
        return $a['parent_id'] < $b['parent_id'] ? -1 : 1;
    }
    
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
