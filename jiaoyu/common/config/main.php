<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'elastic'=>[
            'class' => 'elasticsearch\ClientBuilder',
        ],
         'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=juai',
            'username' => 'root',
            'password' => 'toor',
            'charset' => 'utf8',
        ],
    ],
    'aliases'=>[
        'frontend_webroot'=>'@frontend/web',
        'backend_webroot'=>'@backend/web'
    ]
];
