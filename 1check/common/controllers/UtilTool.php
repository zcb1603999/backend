<?php
namespace common\controllers;

use Yii;

trait UtilTool{
    public static $elasticObj;
    public static function initObj(){
        self::$elasticObj = Yii::$app->elastic->create()->setHosts(Yii::$app->params['elasticseach-host'])->build();
    }
}