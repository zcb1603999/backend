<?php
namespace backend\components;

use Yii;
use yii\base\Widget;
use backend\models\Resource;

class MenuselectorWidget extends Widget {
    
    public $selected_resources=[];
    
    public function init() {
        parent::init();
    }
    
    public function run(){
        $result=Resource::find()->asArray()->orderBy(['addtime'=>SORT_ASC])->all();
        $menu=Resource::toArrayMenu($result);
        return $this->render('_menuselector',[
            'menu'=>$menu,
            'selected_resources'=>$this->selected_resources
        ]);
    }
}
