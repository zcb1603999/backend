var myApp = new Framework7();
var $$ = Dom7;

var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true
});
myApp.onPageInit('about', function (page) {
    $$('.create-page').on('click', function () {
        createContentPage();
    });
});
myApp.swiper('.yicha_banner', {
  pagination:'.yicha_banner .swiper-pagination',
  spaceBetween: 50
});

$$('#search_form').on('beforeSubmit',function(e){
    myApp.showPreloader('正在努力查询结果');
});

$$('#search_form').on('submitted',function(e){
    var html = e.detail.data;
    if(html){
        $$('.yicha_banner').hide();
        $$('.mini_store').hide();
        $$('.recommend').hide();
        
        $$('.yicha_block_area.qrcode').show();
        $$('.yicha_block_area.product').show();
        $$('.yicha_search_content').html(html);
    }
    myApp.hidePreloader();
});
$$('#search_form').on('submitError',function(){
    myApp.hidePreloader();
    myApp.alert("查询失败",'提示');
});

$$(document).on('click','.more_kd_info',function(){
    var me=$$(this);
    if(me.attr('data')=='merge'){
        $$('.kd_other_block').show();
        me.attr('data','expand');
        me.find('i').html('chevron_up');
    }else if(me.attr('data')=='expand'){
        $$('.kd_other_block').hide();
        me.attr('data','merge');
        me.find('i').html('chevron_down');
    }
});

$$(document).on('click','.kd_number_list',function(){
    var me=$$(this);
    $$('.kd_tag').hide(); 
    $$('#kd_tag_'+me.find('input').val()).show();
});
$$('#sign_form').on('submitted',function(e){
    var data = e.detail.data;
    myApp.hidePreloader();
});
$$('#sign_form').on('submitError',function(){
    myApp.hidePreloader();
    myApp.alert("注册失败","提示");
});
$$('#login_form').on('beforeSubmit',function(e){
    myApp.showPreloader('正在登录');
});
$$('#login_form').on('submitted',function(e){
    myApp.hidePreloader();
    info=JSON.parse(e.detail.data);
    if(info.status!='success'){
        myApp.alert(info.reason, '错误提示');
    }else{
        document.location.reload();
    }
});
$$('#login_form').on('submitError',function(){
    myApp.hidePreloader();
    myApp.alert("登录失败","提示");
});