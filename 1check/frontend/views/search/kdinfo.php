
<?php if(!empty($data)) { ?>

<?php if(count($data)>1){ ?>
<div class="content-block yicha_block_area yicha_search_multikd">
    <ul class="list-block">
        <?php $i=1;foreach($data as $kdnumber=>$trace) { ?>
            <li>
                <label class="label-radio item-content kd_number_list">
                    <input type="radio" name="kd_list" value="<?=$kdnumber?>" <?=$i==1?'checked="checked"':''?>>
                        <div class="item-inner">
                        <div class="item-title">(<?=$trace['kd_cp']['name']?>)<?=$kdnumber?></div>
                    </div>
                </label>
            </li>
        <?php $i++;} ?>
    </ul>
</div>
<?php } ?>

<?php $j=1;foreach($data as $kdnumber=>$trace) { ?>
<div class="kd_tag" id="kd_tag_<?=$kdnumber?>" style="<?=$j>1?'display:none':''?>">
    <div class="content-block yicha_block_area yicha_search_result">
        <div class="content-block-inner">
            <ul class="kd_info">
                <?php $count=count($trace['info']);foreach($trace['info'] as $k=>$v){ ?>
                    <li class="kd_block <?=$k==0?'kd_block_last':''?> <?=$k==($count-1)?'':'kd_other_block'?>">
                        <i class="<?=$k==($count-1)?'node_key':'node'?>"><div class="node_inner"></div></i>
                        <div class="content"><?=$v['AcceptStation']?></div>
                        <div class="time"><?=$v['AcceptTime']?></div>
                    </li>
                <?php } ?>

                <?php if($count>1){ ?>
                <li class="more_kd_info" data="merge">
                    <i class="f7-icons">chevron_down</i>
                </li>
                <?php }?>
            </ul>
        </div>
    </div>
</div>
<?php $j++;} ?>
<?php } ?>