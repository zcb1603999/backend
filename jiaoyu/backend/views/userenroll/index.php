<?php

use common\models\UserEnroll;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = '报名列表';
?>
<div class="user-enroll-index">
    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'第{page}/{pageCount}页，本页{count}条，共{totalCount}条',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'baby_sex',
                'value'=>function($model){
                    return UserEnroll::babysexDropDown($model->baby_sex);
                },
                'filter'=>UserEnroll::babysexDropDown()
            ],
            'parent_name',
            'parent_phone',
            [
                'attribute'=>'enroll_type',
                'value'=>function($model){
                    return UserEnroll::enrolltypeDropDown($model->enroll_type);
                },
                'filter'=>UserEnroll::enrolltypeDropDown()
            ],
            'addtime',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作', 
                'template'=>'{view} {delete}',
            ],
        ],
    ]); ?>
</div>
