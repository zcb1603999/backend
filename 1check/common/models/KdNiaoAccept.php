<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kd_niao_accept".
 *
 * @property integer $id
 * @property integer $kd_info_id
 * @property string $request_type
 * @property string $request_data
 */
class KdNiaoAccept extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kd_niao_accept';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_info_id'], 'required'],
            [['kd_info_id'], 'integer'],
            [['request_data'], 'string'],
            [['request_type'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_info_id' => 'Kd Info ID',
            'request_type' => 'Request Type',
            'request_data' => 'Request Data',
        ];
    }
}
