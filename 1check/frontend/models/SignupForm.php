<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $user_phone;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_phone', 'filter', 'filter' => 'trim'],
            ['user_phone', 'required'],
            ['user_phone', 'unique', 'targetClass' => '\common\models\User', 'message' => '手机已注册'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            $error=$this->getErrors();
            print_r($error);
            return null;
        }
        
        $user = new User();
        $user->user_phone = $this->user_phone;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
