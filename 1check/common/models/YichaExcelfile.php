<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "yicha_excelfile".
 *
 * @property integer $id
 * @property integer $store_user_id
 * @property string $file_path
 * @property string $addtime
 */
class YichaExcelfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yicha_excelfile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_user_id'], 'integer'],
            [['addtime'], 'safe'],
            ['addtime', 'default','value'=>date('Y-m-d H:i:s')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_user_id' => 'Store User ID',
            'file_path' => '上传excel',
            'addtime' => '上传时间',
        ];
    }
}
