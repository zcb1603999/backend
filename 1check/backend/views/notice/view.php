<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\YichaNotice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Yicha Notices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yicha-notice-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'store_user_id',
            'content',
            'orders',
        ],
    ]) ?>

</div>
