<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = ['label' => '推荐产品列表', 'url' => ['index']];
$this->params['breadcrumbs'][] = '修改推荐产品';
?>
<div class="yicha-recommand-product-update">

    <?= Breadcrumbs::widget(['homeLink'=>['label'=>'首页','url' => Yii::$app->homeUrl],'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?> 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
