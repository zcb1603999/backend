<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserKdInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Kd Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-kd-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Kd Info', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'store_user_id',
            'user_name',
            'phone',
            'province',
            // 'city',
            // 'area',
            // 'address',
            // 'kd_number',
            // 'kd_company_id',
            // 'product_name',
            // 'send_user',
            // 'remark',
            // 'addtime',
            // 'kd_status',
            // 'kd_info:ntext',
            // 'is_subscribe',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
