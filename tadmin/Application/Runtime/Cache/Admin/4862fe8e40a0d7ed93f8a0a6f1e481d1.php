<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=">
<title>后台管理系统</title>
<link rel="stylesheet" href="/auth/Public/Admin/css/css.css">
<script src="/auth/Public/Admin/js/jquery-1.9.1.js"></script>
<script src="/auth/Public/Common/Layer-1.9.3/layer.js"></script>
<script>
function check(){
	var group_id = $('#group_id').val();
	if(group_id == ''){
		layer.tips('请选择所属用户组', '#group_id');
		return false;
	}
	return true;
}
</script>
</head>
<body>
<div class="nav">
	<div class="nav_title">
    	<h2><img class="nav_img" src="/auth/Public/Admin/img/tab.gif" /><span class="nav_a">编辑用户</span></h2>
    </div>
    <div class="nav_button">
    	<a href="<?php echo U('Admin/admin_list');?>"><button class="button">返回上一层</button></a>
    </div>
</div>
<form name="myform" action="/auth/index.php/Admin/Admin/admin_edit/id/32.html" method="post" onsubmit="return check();">
<div class="list">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="details">
        <tbody>
	      <tr>
	      	<td width="17%"><label class="label">
	      	  <div align="right">账号：</div>
	      	</label></td>
	      	<td width="83%"><?php echo ($vo["account"]); ?></td>
	      </tr>
          <tr>
            <td><div align="right">用户组：</div></td>
            <td><select name="group_id" id="group_id" class="select">
            <option value="<?php echo ($vo["group_id"]); ?>"><?php echo ($vo["title"]); ?></option>
            <?php if($_SESSION['aid'] == 1): ?><option value="">---- 请选择所属组 ----</option>
            <?php if(is_array($group)): foreach($group as $key=>$vo2): ?><option value="<?php echo ($vo2["id"]); ?>"><?php echo ($vo2["title"]); ?></option><?php endforeach; endif; endif; ?>
            </select></td>
          </tr>
          <tr>
            <td><div align="right">Email：</div></td>
            <td><input type="text" name="email" id="email" size="40" value="<?php echo ($vo["email"]); ?>" /></td>
          </tr>
          <tr>
            <td><div align="right">手机号：</div></td>
            <td><input type="text" name="mobile" id="mobile" size="40" value="<?php echo ($vo["mobile"]); ?>" /></td>
          </tr>
          <tr>
            <td><div align="right">最近登录IP：</div></td>
            <td><?php echo ($vo["login_ip"]); ?></td>
          </tr>
          <tr>
            <td><div align="right">最近登录时间：</div></td>
            <td><?php echo (date('Y-m-d H:i:s',$vo["login_time"])); ?></td>
          </tr>
          <tr>
            <td><div align="right">登录次数：</div></td>
            <td><?php echo ($vo["login_count"]); ?></td>
          </tr>
          <tr>
            <td><div align="right">创建时间：</div></td>
            <td><?php echo (date('Y-m-d H:i:s',$vo["create_time"])); ?></td>
          </tr>  
          <tr>
            <td><div align="right">账号状态：</div></td>
            <td>
            <?php if($_SESSION['aid'] == 1): ?><input name="status" type="radio" id="qiyong" value="1" style="position:relative;top:8px;"  <?php if($vo["status"] == 1): ?>checked<?php endif; ?> />
            <label for="qiyong">启用</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input name="status" type="radio" id="jinyong" value="0" style="position:relative;top:8px;"  <?php if($vo["status"] == 0): ?>checked<?php endif; ?> />
            <label for="jinyong">禁用</label>
            <?php else: ?>
            正常<input name="status" type="hidden" value="1" /><?php endif; ?>
            </td>
          </tr>
          
        </tbody>
  </table>
</div>

<div class="footer">
	<div class="save_button">
    	<input type="hidden" name="id" value="<?php echo ($vo["id"]); ?>" />
        <button type="submit" class="button" style="width:180px;">保 存</button>
    </div>
</div>
</form>

</body>
</html>