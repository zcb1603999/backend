<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\MenuWidget;

AppAsset::register($this);
?>

<?php $this->title='后台管理'; ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">	
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<?= Html::csrfMetaTags() ?>
    	<title><?= Html::encode($this->title) ?></title>
    	<?php $this->head(); ?>	
	</head>
	
	<body>
		<?php $this->beginBody() ?>
			<div class="container-fluid">
				
				<div class="row">
					<div class="col-md-12">
						<nav class="navbar navbar-default">
							<div class="container-fluid">
						    	<div class="navbar-header">
						      		<a class="navbar-brand" href="#"><?= Html::encode($this->title) ?></a>
						    	</div>
						    	<ul class="nav navbar-nav navbar-right">
							   		<li class="dropdown">
							          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">用户<span class="caret"></span></a>
							          <ul class="dropdown-menu">
							            <li><a href="<?php echo Url::toRoute(['user/logout']); ?>">登出</a></li>
							          </ul>
							        </li>
							   	</ul>
						   	</div>
						   	
					   	</nav>
					</div>
				</div>
				
				<div class="row">
				  	<div class="col-md-2">
				  		<!-- 菜单部分 -->
				  		<?=MenuWidget::widget()?>
				  	</div>
				  	<div class="col-md-10"><?= $content ?></div>
				</div>
				
			</div>
		<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>
