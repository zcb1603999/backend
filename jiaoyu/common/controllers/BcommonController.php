<?php
/**
 * 后台基类
 */
namespace common\controllers;

use Yii;
use yii\web\Controller;
use common\controllers\UtilTool;
use yii\data\Pagination;

class BcommonController extends Controller{
    use UtilTool;
    
    public $layout = 'backend_layout';
    protected  $pagination = null;
    private $pageSize = 10;


    public function init(){
        UtilTool::initObj();
        $this->pagination = new Pagination();
        $this->pagination->setPageSize($this->pageSize);
        
        if(!Yii::$app->user->id){
            return $this->redirect('site/login');
        }
    }
}
