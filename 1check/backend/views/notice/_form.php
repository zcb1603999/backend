<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="yicha-notice-form">

    <?php $form = ActiveForm::begin([
            'options'=>[
                'class'=>'form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
            'fieldConfig'=>[
                'template'=>'{label}<div class="col-lg-7">{input}</div>{error}',
                'labelOptions'=>['class' => 'col-lg-1 control-label']
            ]]); ?>

    <?= $form->field($model, 'content')->textInput() ?>

    <?= $form->field($model, 'orders',['template'=>'{label}<div class="col-lg-2">{input}</div>{error}'])->textInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-1  col-lg-3">
        <?= Html::submitButton($model->isNewRecord ? '新建' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
