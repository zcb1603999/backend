<?php
namespace backend\models;

use yii\base\Model;
use backend\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
	public $telphone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required','message'=>'必须填写用户名'],
            ['username', 'string', 'min' => 2, 'max' => 128],
            ['email', 'filter', 'filter' => 'trim'],
            ['telphone', 'string', 'min'=> 1, 'max'=>'15'],
            ['telphone', 'required','message'=>'必须填写手机'],
            ['telphone', 'unique', 'targetClass' => '\backend\models\User', 'message' => '该手机已被注册使用'],
            ['password', 'required','message'=>'必须填写密码'],
            ['password', 'string', 'min' => 6],
        ];
    }
	
	public function attributeLabels()
    {
        return [
            'username' => '用户名',
            'password' => '密码',
            'email' => '邮箱',
            'telphone' => '手机'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
	$user->telphone=$this->telphone;
        $user->setPassword($this->password);
        $user->generateAuthKey();
		
        return $user->save() ? $user : null;
    }
}
